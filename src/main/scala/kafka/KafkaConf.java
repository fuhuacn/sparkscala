package kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * created by wangzhi 2019-03-28 17:11
 **/
public class KafkaConf {
    private final static Logger logger = LoggerFactory.getLogger(KafkaConf.class);
    private final static String KAFKA_CONF_FILE = "kafka-producer.properties";
    private final static String KAFKA_CONSUMER_CONF_FILE = "kafka-consumer.properties";
    private final static String KAFKA_ADMIN_CONF_FILE = "kafka-admin.properties";


    private static Properties kafkaConf = null;
    private static Properties kafkaConsumerConf = null;
    private static Properties kafkaAdminConf = null;


    public static Properties getKafkaConf() {
        if (null == kafkaConf) {
            kafkaConf = loadKafkaConf(KAFKA_CONF_FILE);
        }
        return kafkaConf;
    }

    public static Properties getKafkaConsumerConf() {
        if (null == kafkaConsumerConf) {
            kafkaConsumerConf = loadKafkaConf(KAFKA_CONSUMER_CONF_FILE);
        }
        return kafkaConsumerConf;
    }

    public static Properties getKafkaAdminConf() {
        if (null == kafkaAdminConf) {
            kafkaAdminConf = loadKafkaConf(KAFKA_ADMIN_CONF_FILE);
        }
        return kafkaAdminConf;
    }

    public static Properties loadKafkaConf(String propName) {
        Properties properties = new Properties();
        try {
            InputStream is = KafkaConf.class.getClassLoader().getResourceAsStream(
                    propName);

            properties.load(is);
        } catch (IOException e) {
            logger.error("Load kafka configuration error!");
        }
        return properties;
    }
}
