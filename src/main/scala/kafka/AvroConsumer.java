package kafka;

import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

/**
 * created by wangzhi 2019-05-16 9:13
 **/
public class AvroConsumer {

    private static void autoCommitOffset(String groupId) {
        Properties prop = KafkaConf.getKafkaConsumerConf();
        prop.put("key.deserializer", "io.confluent.kafka.serializers.KafkaAvroDeserializer");
        prop.put("value.deserializer", "io.confluent.kafka.serializers.KafkaAvroDeserializer");
        prop.put("schema.registry.url", "http://192.168.34.62:8081");
        if (groupId != null) {
            prop.put("group.id", groupId);
        }
        try (KafkaConsumer<String, GenericRecord> consumer = new KafkaConsumer<>(prop)) {
            consumer.subscribe(Collections.singletonList("lol-test"));
            while (true) {
                ConsumerRecords<String, GenericRecord> records = consumer.poll(Duration.ofSeconds(10, 0));
                for (ConsumerRecord<String, GenericRecord> record : records){
                    System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());
                    GenericRecord record1 = record.value();
                    record1.getSchema().getFields().get(1).schema().getType();
                    record1.get("name");
                }

            }
        }
    }

    public static void main(String[] args) {
        String groupId = args[0];
        autoCommitOffset(groupId);
    }

}
