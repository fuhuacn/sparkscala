package kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * created by wangzhi 2019-03-28 16:26
 **/
public class ConsumerDemo {

    private static void autoCommitOffset() {
        Properties prop = KafkaConf.getKafkaConsumerConf();
        try (KafkaConsumer<String, String> consumer = new KafkaConsumer<>(prop)) {
            consumer.subscribe(Collections.singletonList("my-topic"));
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(10, 0));
                for (ConsumerRecord<String, String> record : records)
                    System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());
            }
        }
    }

    private static void manualCommitOffset() {
        Properties prop = KafkaConf.getKafkaConsumerConf();
        prop.put("enable.auto.commit", "false");
        final int minBatchSize = 50;
        List<ConsumerRecord<String, String>> buffer = new ArrayList<>();
        try (KafkaConsumer<String, String> consumer = new KafkaConsumer<>(prop)) {
            consumer.subscribe(Collections.singletonList("my-topic"));
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(10, 0));
                for (ConsumerRecord<String, String> record : records) {
                    buffer.add(record);
                }
                if (buffer.size() >= minBatchSize) {
//                insertIntoDb(buffer);
                    // 手动提交offset
                    System.out.println(buffer.get(buffer.size() - 1).offset());
                    consumer.commitSync();
                    buffer.clear();
                }
            }
        }
    }

    private static void manualCommitPartitonOffset() {
        Properties prop = KafkaConf.getKafkaConsumerConf();
        prop.put("enable.auto.commit", "false");
        final int minBatchSize = 50;
        List<ConsumerRecord<String, String>> buffer = new ArrayList<>();
        try (KafkaConsumer<String, String> consumer = new KafkaConsumer<>(prop)) {
            consumer.subscribe(Collections.singletonList("my-topic"));
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(10, 0));
                for (TopicPartition partition : records.partitions()) {
                    List<ConsumerRecord<String, String>> partitionRecords = records.records(partition);
                    for (ConsumerRecord<String, String> record : partitionRecords) {
                        System.out.println(record.offset() + ": " + record.value());
                    }
                    long lastOffset = partitionRecords.get(partitionRecords.size() - 1).offset();
                    consumer.commitSync(Collections.singletonMap(partition, new OffsetAndMetadata(lastOffset + 1)));
                }
            }
        }
    }

    public static void main(String[] args) {
        manualCommitPartitonOffset();
    }
}
