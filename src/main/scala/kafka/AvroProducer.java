package kafka;

import kafka.model.Message;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.Future;

/**
 * created by wangzhi 2019-04-24 15:24
 **/
public class AvroProducer {

    /**
     * 同步发送
     */
    public static void syncSend() {
        Properties props = KafkaConf.loadKafkaConf("kafka-avro-producer.properties");
        Producer<String, Message> producer = new KafkaProducer<>(props);
        Future<RecordMetadata> future = null;
        String topicName = "SparkStreaming-test-temp";
        List<Message> messages = new ArrayList<>();
        Map map = new HashMap();
        String[] strs = "99962,1389232920000,4.77072,-13.56189,-8.61108,7.83416,16.58798,25.16828,20.43314,4.13638,61.51511,2.80841,51.19782,8.33330,17.26904,6.87082,38.17301,23.99975,71.29941,-6.50730,-1.39670,11.00080,35.45954,12.98783,43.99217".split(",");
        for (int i=0;i<strs.length;i++){
            map.put(String.valueOf(i),strs[i]);
        }
        for (int i = 0; i < 1; i++){
            Message message = new Message();
            message.setTopic(topicName);
            message.setCreatedTime(System.currentTimeMillis());
            message.setOffset((long)i);
            message.setContent(ByteBuffer.wrap(("NO. "+i).getBytes()));
            message.setProps(map);
            messages.add(message);
        }

        for (int i = 0; i < messages.size(); i++){
            Message message = messages.get(i);
            if (i == messages.size() - 1) {
                future = producer.send(new ProducerRecord<>(topicName, Long.toString(message.getCreatedTime()), message));
            }else{
                producer.send(new ProducerRecord<>(topicName, Long.toString(message.getCreatedTime()), message));
            }
        }
        if (future != null) {
            try {
                future.get();
                System.out.println("sync sent!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        producer.close();
    }

    public static void main(String[] args) {
        syncSend();
    }

}
