package kafka;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.ListConsumerGroupsResult;
import org.apache.kafka.clients.admin.ListTopicsResult;

import java.util.concurrent.ExecutionException;

/**
 * created by wangzhi 2019-03-28 23:32
 **/
public class KafkaAdminDemo {

    public static void main(String[] args) {
        try(AdminClient adminClient = AdminClient.create(KafkaConf.getKafkaAdminConf())) {
            ListTopicsResult topicsResult = adminClient.listTopics();
            System.out.println(topicsResult.names().get());
            ListConsumerGroupsResult listTopicsResult = adminClient.listConsumerGroups();
            System.out.println(listTopicsResult.all().get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
