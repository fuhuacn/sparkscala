package kafka;


import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.Future;

/**
 * created by wangzhi 2019-03-28 16:43
 **/
public class ProducerDemo {

    private static final int SEND_SIZE = 10;

    /**
     * 简单发送到kafka，不关心结果
     */
    public static void send() {
        Properties props = KafkaConf.getKafkaConf();
        Producer<String, String> producer = new KafkaProducer<>(props);
        Random random = new Random();
        Map map = new HashMap();
//        for (int i = 0; i < SEND_SIZE; i++){
//            map.put("社交",random.nextInt(100));
//            map.put("阅读",random.nextInt(10));
//            producer.send(new ProducerRecord<>("my-topic2", Long.toString(System.currentTimeMillis()), JSONObject.fromObject(map).toString()));
//        }
//        map.put("社交",random.nextInt(5));
//        map.put("阅读",random.nextInt(100));
        producer.send(new ProducerRecord<>("my-topic2", "24", "0"));
        producer.send(new ProducerRecord<>("my-topic2", "24", "99"));
        producer.send(new ProducerRecord<>("my-topic2", "0", "0"));

        producer.close();
    }

    /**
     * 同步发送
     */
    public static void syncSend() {
        Properties props = KafkaConf.getKafkaConf();
        Producer<String, String> producer = new KafkaProducer<>(props);
        Future<RecordMetadata> future = null;
        for (int i = 0; i < SEND_SIZE; i++){
            if (i == SEND_SIZE - 1) {
                future = producer.send(new ProducerRecord<>("my-topic", Long.toString(System.currentTimeMillis()), Integer.toString(i)));
            }else{
                producer.send(new ProducerRecord<>("my-topic", Long.toString(System.currentTimeMillis()), Integer.toString(i)));
            }
        }
        if (future != null) {
            try {
                future.get();
                System.out.println("sync sent!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        producer.close();
    }


    /**
     * 异步发送
     */
    public static void asyncSend() {
        Properties props = KafkaConf.getKafkaConf();
        Producer<String, String> producer = new KafkaProducer<>(props);
        producer.send(new ProducerRecord<>("my-topic", Long.toString(System.currentTimeMillis()), Integer.toString(500)),
                (recordMetadata, e) -> {
            if (e != null) {
                e.printStackTrace();
            } else {
                System.out.println("sent!");
            }
        });

        producer.close();
    }

    public static void main(String[] args) {
        send();
    }
}
