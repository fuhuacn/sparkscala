package kafka;

import kafka.model.Message;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.Future;

/**
 * created by wangzhi 2019-04-24 15:24
 **/
public class AvroProducer3 {

    /**
     * 同步发送
     */
    public static void main(String[] args) throws Exception{
        Properties props = KafkaConf.loadKafkaConf("kafka-avro-producer.properties");
        Producer<String, Message> producer = new KafkaProducer<>(props);
        Future<RecordMetadata> future = null;
        String topicName = "SparkStreaming-test5";
        List<Message> messages = new ArrayList<>();

        File file = new File("/opt/2.csv");
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String tempString = null;
        int line = 0;
        int startLine = Integer.parseInt(args[0]);//比最后一个大2
        int endLine = startLine+1440;//比真实大1
        // 一次读入一行，直到读入null为文件结束
        while ((tempString = reader.readLine()) != null) {
            // 显示行号
            if (line<startLine){
                line++;
                continue;
            }
            if (line>endLine) break;
            System.out.println("line " + line);
            Message message = new Message();
            message.setTopic(topicName);
            message.setCreatedTime(System.currentTimeMillis());
            message.setOffset((long) line);
            message.setContent(ByteBuffer.wrap(("NO. " + line).getBytes()));
            Map map = new HashMap();
            String[] strs = tempString.split(",");
            for (int j = 0; j < strs.length; j++) {
                map.put(String.valueOf(j), strs[j]);
            }
            message.setProps(map);
            messages.add(message);
            line++;
        }
        reader.close();

        for (int i = 0; i < messages.size(); i++) {
            Message message = messages.get(i);
            if (i == messages.size() - 1) {
                future = producer.send(new ProducerRecord<>(topicName, Long.toString(message.getCreatedTime()), message));
            } else {
                producer.send(new ProducerRecord<>(topicName, Long.toString(message.getCreatedTime()), message));
            }
        }
        if (future != null) {
            try {
                future.get();
                System.out.println("sync sent!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        producer.close();
    }

}
