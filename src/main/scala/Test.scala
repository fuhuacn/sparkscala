package com.free4lab.sparktest

import java.io.FileWriter
import java.sql.{Connection, DriverManager, PreparedStatement, ResultSet, Statement}

import com.free4lab.sparktest.operation.WriteHDFS
import kafka.AvroProducer2
import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.DoubleType

/**
 * @Author: fuhua
 * @Date: 2019-06-12 16:17
 *        窗口滑动处理
 */
object Test {
  def main(args: Array[String]): Unit = {
    /*
     * 以下是批处理
     */
    val spark = SparkSession.builder().master("local[2]").appName("test").getOrCreate()

        val datasetOrOrgin = spark.read.format("csv").option("header", "true").load("/Users/fuhua/Desktop/cardguiyihua.csv")


    import spark.implicits._

    val datasetOr = datasetOrOrgin.filter($"Class" =!= "0").orderBy($"_c0".cast(DoubleType).asc)
    datasetOr.foreach(row=>{
      val out = new FileWriter("/Users/fuhua/Desktop/sql.out",true)
      out.write("'"+row.getString(0)+"',")
      out.close()
    })
  }
}
