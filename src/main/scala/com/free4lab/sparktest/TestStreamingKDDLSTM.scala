//package com.free4lab.sparktest
//
//import java.io.File
//import java.sql.{Connection, PreparedStatement}
//
//import com.free4lab.sparktest.constants.{CommonMethods, Con}
//import com.free4lab.sparktest.operation.{MysqlOperation, WriteHDFS}
//import org.apache.avro.generic.GenericRecord
//import org.apache.avro.util.Utf8
//import org.apache.spark.sql.types.{IntegerType, LongType}
//import org.apache.spark.sql.{SaveMode, SparkSession}
//import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
//import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
//import org.apache.spark.streaming.kafka010.{HasOffsetRanges, KafkaUtils, OffsetRange}
//import org.apache.spark.streaming.{Seconds, StreamingContext}
//import org.apache.spark.{SparkConf, TaskContext}
//import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
//import org.nd4j.linalg.factory.Nd4j
//
///**
// * @Author: fuhua
// * @Date: 2019-06-13 11:57
// *        告警大于8倍基准值，会存到mysql
// */
//object TestStreamingKDDLSTM extends Serializable {
//
//
//  def main(args: Array[String]): Unit = {
//    /*
//     * 以下是流处理
//     */
//    val pattern = ","
//    val conf = new SparkConf().setMaster("spark://ambari-namenode.com:7077").setAppName("Streaming")
//    //    val conf = new SparkConf().setMaster("local[2]").setAppName("Streaming")
//    val ssc = new StreamingContext(conf, Seconds(2))
//
//
//    val kafkaParams = Map[String, Object](
//      "bootstrap.servers" -> "192.168.34.60:9092,192.168.34.62:9092,192.168.34.57:9092,192.168.34.79:9092,192.168.34.80:9092",
//      //      "key.deserializer" -> classOf[StringDeserializer],
//      //      "value.deserializer" -> classOf[StringDeserializer],
//      "key.deserializer" -> classOf[io.confluent.kafka.serializers.KafkaAvroDeserializer],
//      "value.deserializer" -> classOf[io.confluent.kafka.serializers.KafkaAvroDeserializer],
//      "schema.registry.url" -> "http://192.168.34.62:8081",
//      "group.id" -> "streamTest",
//      "auto.offset.reset" -> "latest",
//      "enable.auto.commit" -> (false: java.lang.Boolean)
//    )
//
//    val topics = Array("SparkStreaming-test-temp")
//
//    val stream = KafkaUtils.createDirectStream[String, String](
//      ssc,
//      PreferConsistent,
//      Subscribe[String, String](topics, kafkaParams)
//    )
//
//    val spark = SparkSession
//      .builder
//      .appName("SparkStreaming-test-temp")
//      .getOrCreate()
//
//    /*
//1    * 管理offset
//     */
//    stream.foreachRDD { rdd =>
//      val offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
//      rdd.foreachPartition { iter =>
//        val o: OffsetRange = offsetRanges(TaskContext.get.partitionId)
//        println(s"${o.topic} ${o.partition} ${o.fromOffset} ${o.untilOffset}")
//      }
//    }
//
//    import org.apache.spark.sql.functions._
//    import spark.implicits._
//
//    /*
//     * 告警
//     */
//    stream.map(p => UnitKafka(p.value().asInstanceOf[GenericRecord]).id_attrs).foreachRDD(rdd => {
//      if (!rdd.isEmpty()) {
//        println("Data is coming!")
//        val sameModel: MultiLayerNetwork = CommonMethods.loadLSTMModel(Con.LSTM_FILE_NAME)
//        if (sameModel == null) {
//          println("Streaming null model")
//        }
//        if (sameModel != null) {
//          println("Begin to check!")
//          val wssd: Double = MysqlOperation.readValue("wssd", "wssd", "topic='SparkStreaming-test-temp-biaozhuncha'")
//          val wssdmeans: Double = MysqlOperation.readValue("wssd", "wssd", "topic='SparkStreaming-test-temp-means'")
//          val wssdmaxP: Double = MysqlOperation.readValue("wssd", "wssd", "topic='SparkStreaming-test-temp-maxP'")
//
//
//          val beginTime = System.currentTimeMillis()
//          val length = rdd.first()._2.split(pattern).length - 2
//          //        +--------------------+-------------+-----+--------------------+
//          //        |                data|         time|index|               topic|
//          //        +--------------------+-------------+-----+--------------------+
//          //        |99962,13892329200...|1389232920000|99962|SparkStreaming-te...|
//          //        +--------------------+-------------+-----+--------------------+
//          var df_latest = MysqlOperation.readDF(spark, "latest")
//          df_latest = df_latest.orderBy($"index".cast(IntegerType).asc).limit(1).select("index", "data")
//          //        +-----+--------------------+
//          //        |   _1|                  _2|
//          //        +-----+--------------------+
//          //        |99962|99962,13892329200...|
//          //        +-----+--------------------+
//          val df = rdd.toDF("index", "data")
//          val df_final = df_latest.union(df)
//          val df_final2 = df_final
//          df_final.createOrReplaceTempView("df_final")
//          df_final2.createOrReplaceTempView("df_final2")
//          val df_result = spark.sql("select df_final.data as features,df_final2.data as labels from df_final inner join df_final2 on df_final.index = df_final2.index-1")
//
//          df_result.foreachPartition(partition => {
//            val conn: Connection = MysqlOperation.getConnect
//            var ps: PreparedStatement = null
//            println("A new partition")
//            partition.foreach(row => {
//              val temp = row.getString(0) //拿预测值
//              val temps = temp.split(",")
//              val predictValue = Nd4j.zeros(1, length, 1)
//              for (i <- 2 until temps.length-1) {
//                predictValue.putScalar(Array(1, i - 2, 1), temps(i).toDouble)
//              }
//              val predict = sameModel.output(predictValue)
//              val reals = row.getString(1).split(pattern)
//              var maxPercent: Double = 0.0 //比较最大比值
//              var sum: Double = 0
//              var percents: Seq[Double] = Seq[Double]()
//              for (i <- 1 until temps.length-1) {
//                val eachPredictValue = predict.getFloat(Array(1, i - 1, 1))
//                //差值除以总值获得percent,这个有正有负
//                var percentage:Double = 0
//                if (reals(i).toDouble != 0) {
//                  percentage = (eachPredictValue -reals(i).toDouble)  / reals(i).toDouble
//                }else{
//                  percentage = (1 + eachPredictValue -reals(i).toDouble) / (reals(i).toDouble + 1)
//                  println("real is 0")
//                }
//                sum += percentage
//                percents = percents :+ percentage
//              }
//              val means = sum / length //理想情况应该是0，仅算方差用
//              var fangcha: Double = 0
//              var allzhengshuSum:Double = 0
//              for (i <- 1 until percents.length-1) {
//                val eachPredictValue = predict.getFloat(Array(1, i - 1, 1))
//                print(eachPredictValue + ",")
//                fangcha += Math.pow(percents(i-1) - means, 2)
//                var abs = Math.abs(percents(i-1))
//                allzhengshuSum += abs //将所有的percents转正
//                if(abs> maxPercent){
//                  maxPercent = abs
//                }
//                print(abs + ",")
//              }
//              val biaozhucha = Math.pow(fangcha, 0.5)
//              val allzhengshuMeans = allzhengshuSum/length
//              println()
//              println(reals.toSeq)
//              println("!searchP " + temps(0) + " 标准差：" + biaozhucha + " 平均数：" + allzhengshuMeans + " 最大比值：" + maxPercent)
//              val countA: Double = biaozhucha * 4 + allzhengshuMeans * 2 + maxPercent
//              if (biaozhucha > wssd || allzhengshuMeans > wssdmeans|| maxPercent > wssdmaxP) {
//                println("\nalarm! ")
//                ps = conn.prepareStatement("insert into `alarm`(`topic`,`index`,`time`,`modelvalue`,`means`,`biaozhuncha`,`maxpercents`) values (?,?,?,?,?,?,?)")
//                ps.setString(1, "")
//                ps.setInt(2, temps(0).toInt)
//                ps.setLong(3, 0)
//                ps.setDouble(4, countA)
//                ps.setDouble(5, allzhengshuMeans)
//                ps.setDouble(6, biaozhucha)
//                ps.setDouble(7, maxPercent)
//                ps.executeUpdate()
//                println("send!")
//              }
//            })
//            MysqlOperation.closeConnect(conn, ps, null)
//          })
//
//          //          df_result.foreach(row => {
//          //
//          //          })
//          println(System.currentTimeMillis() - beginTime)
//          println("search")
//        }
//      }
//    })
//
//    /*
//     * 因为要先取之前的最新数据，这步要在后面执行
//     * 存储至MySQL
//     */
//    stream.map(p => {
//      val unitKafka: UnitKafka = UnitKafka(p.value().asInstanceOf[GenericRecord])
//      (unitKafka.attrs, unitKafka.time, unitKafka.time, unitKafka.index, p.topic())
//    }).foreachRDD(rdd => {
//      if (!rdd.isEmpty()) {
//        val df = rdd.toDF("data", "tag", "time", "index", "topic")
//        MysqlOperation.writeDF(df, "data", SaveMode.Append)
//        val latest = df.orderBy($"time".cast(LongType).desc).limit(1)
//        latest.head(1).foreach(row => {
//          val latestValue: Seq[String] = row.toSeq.asInstanceOf[Seq[String]]
//          var conn: Connection = null
//          var ps: PreparedStatement = null
//          try {
//            conn = MysqlOperation.getConnect
//          }
//          try {
//            ps = conn.prepareStatement("update `latest` set `data`=?,`time`=?,`index`=? where `topic`='" + latestValue(4) + "'")
//            ps.setString(1, latestValue(0))
//            ps.setLong(2, latestValue(2).toLong)
//            ps.setInt(3, latestValue(3).toInt)
//            ps.executeUpdate()
//          }
//          MysqlOperation.closeConnect(conn, ps, null)
//        })
//
//        println("save!")
//      }
//    })
//
//    ssc.start()
//    ssc.awaitTermination()
//
//    case class UnitKafka(gen: GenericRecord) {
//      val propsJava: java.util.Map[String, String] = gen.get("props").asInstanceOf[java.util.Map[String, String]]
//      var attrs = ""
//      var time = "" //在KDD这假设为标签
//      var index = ""
//      for (i <- 0 until propsJava.size()) {
//        var mapKey: Utf8 = new Utf8(String.valueOf(i))
//        val attrEach = propsJava.get(mapKey).asInstanceOf[Utf8].toString
//        attrs += attrEach + pattern
//        if (i == 0) {
//          index = attrEach
//        }
//        if(i==propsJava.size()-1){
//          if(attrEach.equals("normal.")){
//            time = "1"
//          }else{
//            time = "-1"
//          }
//        }
//      }
//      attrs = attrs.substring(0, attrs.length - 1)
//      val id_attrs = (index, attrs)
//    }
//
//
//  }
//}
