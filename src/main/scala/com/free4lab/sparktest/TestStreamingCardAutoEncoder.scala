//package com.free4lab.sparktest
//
//import java.sql.{Connection, PreparedStatement}
//
//import com.free4lab.sparktest.constants.{CommonMethods, Con}
//import com.free4lab.sparktest.operation.MysqlOperation
//import org.apache.avro.generic.GenericRecord
//import org.apache.avro.util.Utf8
//import org.apache.spark.sql.{SaveMode, SparkSession}
//import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
//import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
//import org.apache.spark.streaming.kafka010.{HasOffsetRanges, KafkaUtils, OffsetRange}
//import org.apache.spark.streaming.{Seconds, StreamingContext}
//import org.apache.spark.{SparkConf, TaskContext}
//import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
//import org.nd4j.linalg.factory.Nd4j
//
///**
// * @Author: fuhua
// * @Date: 2019-06-13 11:57
// *        告警大于8倍基准值，会存到mysql
// */
//object TestStreamingCardAutoEncoder extends Serializable {
//
//
//  def main(args: Array[String]): Unit = {
//    /*
//     * 以下是流处理
//     */
//    val pattern = ","
//    val conf = new SparkConf().setMaster("spark://ambari-namenode.com:7077").setAppName("Streaming")
////        val conf = new SparkConf().setMaster("local[2]").setAppName("Streaming")
//    val ssc = new StreamingContext(conf, Seconds(2))
//
//
//    val kafkaParams = Map[String, Object](
//      "bootstrap.servers" -> "192.168.34.60:9092,192.168.34.62:9092,192.168.34.57:9092,192.168.34.79:9092,192.168.34.80:9092",
//      //      "key.deserializer" -> classOf[StringDeserializer],
//      //      "value.deserializer" -> classOf[StringDeserializer],
//      "key.deserializer" -> classOf[io.confluent.kafka.serializers.KafkaAvroDeserializer],
//      "value.deserializer" -> classOf[io.confluent.kafka.serializers.KafkaAvroDeserializer],
//      "schema.registry.url" -> "http://192.168.34.62:8081",
//      "group.id" -> "streamTest",
//      "auto.offset.reset" -> "latest",
//      "enable.auto.commit" -> (false: java.lang.Boolean)
//    )
//
//    val topics = Array("SparkStreaming-test-temp")
//
//    val stream = KafkaUtils.createDirectStream[String, String](
//      ssc,
//      PreferConsistent,
//      Subscribe[String, String](topics, kafkaParams)
//    )
//
//    val spark = SparkSession
//      .builder
//      .appName("SparkStreaming-test-temp")
//      .getOrCreate()
//
//    /*
//1    * 管理offset
//     */
//    stream.foreachRDD { rdd =>
//      val offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
//      rdd.foreachPartition { iter =>
//        val o: OffsetRange = offsetRanges(TaskContext.get.partitionId)
//        println(s"${o.topic} ${o.partition} ${o.fromOffset} ${o.untilOffset}")
//      }
//    }
//
//    import spark.implicits._
//    stream.map(p => {
//      val unitKafka: UnitKafka = UnitKafka(p.value().asInstanceOf[GenericRecord])
//      (unitKafka.attrs, unitKafka.time, 0, unitKafka.index, p.topic())
//    }).foreachRDD(rdd => {
//      if (!rdd.isEmpty()) {
//        val df = rdd.toDF("data", "tag", "time", "index", "topic")
//        MysqlOperation.writeDF(df, "data", SaveMode.Append)
//        println("save!")
//      }
//    })
//    /*
//     * 告警
//     */
//    stream.map(p => UnitKafka(p.value().asInstanceOf[GenericRecord]).id_attrs).foreachRDD(rdd => {
//      if (!rdd.isEmpty()) {
//        println("Data is coming!")
//        val sameModel: MultiLayerNetwork = CommonMethods.loadLSTMModel(Con.AUTOENCODER_NAME)
//        if (sameModel == null) {
//          println("Streaming null model")
//        }
//        if (sameModel != null) {
//          println("Begin to check!")
//          val wssd: Double = MysqlOperation.readValue("wssd", "wssd", "topic='SparkStreaming-test-temp'")
//
//
//          val beginTime = System.currentTimeMillis()
//          val length = rdd.first()._2.split(pattern).length - 2
//
//          val df_final = rdd.toDF("index", "data")
//          df_final.createOrReplaceTempView("df_final")
//          val df_result = spark.sql("select df_final.data as features from df_final")
//
//          df_result.foreachPartition(partition => {
//            val conn: Connection = MysqlOperation.getConnect
//            var ps: PreparedStatement = null
//            println("A new partition")
//            partition.foreach(row => {
//              val temp = row.getString(0) //拿预测值
//              val temps = temp.split(",")
//              val predictValue = Nd4j.create(1, length)
//              for (i <- 2 until temps.length) {
//                predictValue.putScalar(Array(0, i - 2), temps(i).toDouble)
//              }
//              val predict = sameModel.output(predictValue)
//              val reals = temps
//              var sum: Double = 0.0
//              for (i <- 1 until temps.length-1) {
//                sum+=Math.pow((predict.getFloat(Array(1, i - 1)) - reals(i).toDouble),2)
//                print(predict.getFloat(Array(1, i - 1)) + " ")
//              }
//              val mseMeans = sum/(reals.length-2)
//              println("MSE:"+mseMeans)
//              println(reals.toSeq)
//
//              if (mseMeans > wssd ) {
//                println("\nalarm! ")
//                ps = conn.prepareStatement("insert into `alarm`(`topic`,`index`,`modelvalue`) values (?,?,?)")
//                ps.setString(1, "")
//                ps.setInt(2, temps(0).toInt)
//                ps.setDouble(3, mseMeans)
//                ps.executeUpdate()
//                println("send!")
//              }
//            })
//            MysqlOperation.closeConnect(conn, ps, null)
//          })
//
//          //          df_result.foreach(row => {
//          //
//          //          })
//          println(System.currentTimeMillis() - beginTime)
//          println("search")
//        }
//      }
//    })
//
//    ssc.start()
//    ssc.awaitTermination()
//
//    case class UnitKafka(gen: GenericRecord) {
//      val propsJava: java.util.Map[String, String] = gen.get("props").asInstanceOf[java.util.Map[String, String]]
//      var attrs = ""
//      var time = "" //在KDD这假设为标签
//      var index = ""
//      for (i <- 0 until propsJava.size()) {
//        var mapKey: Utf8 = new Utf8(String.valueOf(i))
//        val attrEach = propsJava.get(mapKey).asInstanceOf[Utf8].toString
//        attrs += attrEach + pattern
//        if (i == 0) {
//          index = attrEach
//        }
//        if(i==propsJava.size()-1){
//          if(attrEach.equals("0")){
//            time = "1"
//          }else{
//            time = "-1"
//          }
//        }
//      }
//      attrs = attrs.substring(0, attrs.length - 1)
//      val id_attrs = (index, attrs)
//    }
//
//
//  }
//}
