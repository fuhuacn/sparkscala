package com.free4lab.sparktest.constants

import java.io.File

import com.free4lab.sparktest.operation.WriteHDFS
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork

/**
 * @Author: fuhua
 * @Date: 2019-08-21 16:17
 */
object CommonMethods {
  def loadLSTMModel(name: String): MultiLayerNetwork = {
    try {
      println("read file")
      val ifexist = WriteHDFS.readFile(Con.TEMP_PATH + name, name)
      if (!ifexist) {
        println("no model in hdfs")
        return null
      }
      val file: File = new File(Con.TEMP_PATH + name)
      if (file.exists()) {
        val net = MultiLayerNetwork.load(file, true)
        net
      } else {
        println("只有本地文件，hdfs没有文件")
        null
      }
    } catch {
      case e: Exception => e.printStackTrace()
        null
    }
  }
}
