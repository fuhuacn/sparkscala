package com.free4lab.sparktest.constants

/**
 * @Author: fuhua
 * @Date: 2019-08-15 13:26
 */
object Con {
  val HDFS_PATH = "hdfs://ambari-namenode.com:8020/sintest/"
  val LSTM_FILE_NAME = "lstm.zip"
  val AUTOENCODER_NAME = "autoencoder.zip"

  val MYSQL = "jdbc:mysql://daas.free4inno.com:6033/sparkml"
  val MYSQL_PASS = "MYSQL@free4inno"
  val MYSQL_USER = "root"

  val TEMP_PATH = "/Users/fuhua/Desktop/";
//
//  val TEMP_PATH = "/opt/"
//
//  val FILE_PATH = "hdfs://ambari-namenode.com:8020/sintest/"
  val FILE_PATH = "/Users/fuhua/Desktop/"

}
