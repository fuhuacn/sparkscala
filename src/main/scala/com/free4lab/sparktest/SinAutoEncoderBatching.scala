//package com.free4lab.sparktest
//
//import java.io.File
//import java.sql.{Connection, PreparedStatement}
//
//import com.free4lab.sparktest.constants.{CommonMethods, Con}
//import com.free4lab.sparktest.operation.{MysqlOperation, WriteHDFS}
//import kafka.AvroProducer2
//import org.apache.spark.api.java.JavaRDD
//import org.apache.spark.sql.SparkSession
//import org.apache.spark.sql.types.{DoubleType, IntegerType}
//import org.deeplearning4j.nn.api.OptimizationAlgorithm
//import org.deeplearning4j.nn.conf.NeuralNetConfiguration
//import org.deeplearning4j.nn.conf.layers.{AutoEncoder, OutputLayer, RnnOutputLayer}
//import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
//import org.deeplearning4j.nn.weights.WeightInit
//import org.deeplearning4j.optimize.listeners.ScoreIterationListener
//import org.deeplearning4j.spark.impl.multilayer.SparkDl4jMultiLayer
//import org.deeplearning4j.spark.impl.paramavg.ParameterAveragingTrainingMaster
//import org.nd4j.linalg.activations.Activation
//import org.nd4j.linalg.dataset.DataSet
//import org.nd4j.linalg.factory.Nd4j
//import org.nd4j.linalg.learning.config.Adam
//import org.nd4j.linalg.lossfunctions.LossFunctions
//
///**
// * @Author: fuhua
// * @Date: 2019-06-12 16:17
// *        处理完后发数据
// */
//object SinAutoEncoderBatching {
//  def main(args: Array[String]): Unit = {
//    /*
//     * 以下是批处理
//     */
//    val fileName = "from0test.csv"
////        val fileName = "4.csv"
//
//    val header = ",time,循环氢压缩机出口压力,系统压降,E105高压空冷管程介质入口温度,循环氢压缩机入口压力,E102高压换热器管程入口温度（E101出口）,E101高压换热器壳程入口温度,E201换热器管程出口温度,E102高压换热器管程出口温度,分馏重沸炉入口温度（塔底温度）,E102高压换热器壳程入口温度,分馏重沸炉对流室出口温度,精制反应器入口温度,E201换热器管程入口温度,精制反应器入口压力,E201换热器壳程出口温度,E201换热器壳程入口温度（E102壳出口）,分馏重沸炉排烟温度,E210空冷管程介质入口温度,精制反应器床层总压降,E101高压换热器壳程出口温度,精制反应器平均温度,混氢瞬时流量,冷低分压力\n"
//    val attrLength = header.split(",").length - 2
//    val spark = SparkSession.builder().master("spark://ambari-namenode.com:7077").appName("sinBatching").getOrCreate()
////        val spark = SparkSession.builder().master("local[2]").appName("test").getOrCreate()
//
//    val rownum = MysqlOperation.readValue("rownumber", "rownumber", "id=1")
//    val unit = MysqlOperation.readValue("rownumber", "rownumber", "id=2")
//
//    import spark.implicits._
//    val datasetReadCsv = spark.read.format("csv").option("header", "true").load(Con.FILE_PATH + fileName)
//
//    val datasetOr = datasetReadCsv.orderBy($"_c0".cast(DoubleType).desc).limit((unit + 1).toInt).orderBy($"_c0".cast(DoubleType).asc).map(_.toSeq.foldLeft("")(_ + "," + _).substring(1))
//    datasetOr.show()
//
//
//    datasetOr.createOrReplaceTempView("dataset")
//    val datasetResult = spark.sql("select dataset.value as features from dataset")
//    println(datasetResult.count())
//
//    if (datasetResult.count() < unit) {
//      send(rownum.toInt)
//      return
//    }
//
//    val rddResult: JavaRDD[DataSet] = datasetResult.rdd.map(row => {
//      val featuresOr = row(0).asInstanceOf[String].split(",")
//      val features = Nd4j.create(1, featuresOr.length - 2)
//      val labels = Nd4j.create(1, featuresOr.length - 2)
//      for (i <- 2 until featuresOr.length) {
//        features.putScalar(Array(0, i - 2), featuresOr(i).toDouble)
//        labels.putScalar(Array(0, i - 2), featuresOr(i).toDouble)
//      }
//      new DataSet(features, labels)
//    })
//
//
//    val averagingFrequency = 3
//    val examplesPerDataSetObject = 1
//    val batchSizePerWorker = 50
//    val numEpochs = 1
//
//    val hiddenLayer1Size = 5
//    val conf = new NeuralNetConfiguration.Builder()
//      .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
//      .seed(123)
//      .l2(0.001)
//      .weightInit(WeightInit.XAVIER)
//      .updater(new Adam(0.004))
//      .list()
//      .layer(0, new AutoEncoder.Builder().nIn(attrLength).nOut(hiddenLayer1Size)
//        .activation(Activation.IDENTITY).build())
//      .layer(1, new OutputLayer.Builder(LossFunctions.LossFunction.MSE).activation(Activation.IDENTITY)
//        .nIn(hiddenLayer1Size).nOut(attrLength).build())
//      .build()
//
//
//    val tm = new ParameterAveragingTrainingMaster.Builder(examplesPerDataSetObject)
//      .workerPrefetchNumBatches(2) //Asynchronously prefetch up to 2 batches
//      .averagingFrequency(averagingFrequency)
//      .batchSizePerWorker(batchSizePerWorker)
//      .build()
//
//
//    val sparkNetwork:SparkDl4jMultiLayer =  new SparkDl4jMultiLayer(spark.sparkContext,conf,tm)
//    sparkNetwork.setListeners(new ScoreIterationListener(1))
//
//
//    //    val Array(trainingData,testingData) = rddResult.randomSplit(Array(0.8,0.2))
//    for (j <- 0 until numEpochs) {
//
//      //      val net:MultiLayerNetwork = sparkNetwork.fit(trainingData)
//      println("开始训练！！！！")
//      val net: MultiLayerNetwork = sparkNetwork.fit(rddResult)
//      println("训练结束！！！！")
//
//      val fileCrc: File = new File(Con.TEMP_PATH + "." + Con.AUTOENCODER_NAME + ".crc")
//      if (fileCrc.exists()) {
//        System.out.println("删除crc文件！")
//        fileCrc.delete()
//      }
//      val file: File = new File(Con.TEMP_PATH + Con.AUTOENCODER_NAME)
//      net.save(file, true)
//      WriteHDFS.saveFile(Con.TEMP_PATH + Con.AUTOENCODER_NAME, Con.AUTOENCODER_NAME)
//      //            val trainEvaluation:Evaluation = sparkNetwork.evaluate(trainingData)
//      //      val trainAccuracy = trainEvaluation.accuracy()
//      //      val testEvaluation:Evaluation = sparkNetwork.evaluate(testingData)
//      //      val testAccuracy = testEvaluation.accuracy()
//      //      println(trainAccuracy+" "+testAccuracy)
//      println("train finish")
//
//      //
//            val predictValue2 = Nd4j.create(1, 23)
//            val temp2 = "10245,1383849900000,108.36091,153.65374,136.95855,107.57057,127.33952,136.35189,132.08033,147.12351,103.97628,142.76644,102.55564,101.37406,139.24896,108.34274,139.63569,127.97968,102.62470,139.67914,86.58465,135.45426,134.20369,108.69299,137.23598"
//            val temp2s = temp2.split(",")
//            for (i <- 2 until temp2s.length) {
//              predictValue2.putScalar(Array(0, i - 2), temp2s(i).toDouble)
//            }
//            println()
//
//
//            val output2 = net.output(predictValue2)
//            for (i <- 2 until 25) {
//              print(output2.getFloat(Array(1, i - 2)) + " ")
//            }
//
//    }
//
//    //    val dataWithFeatures = new VectorAssembler()
//    //      .setInputCols(featrueNames)
//    //      .setOutputCol("features")
//    //      .transform(dataset)
//    //
//    //    println(dataWithFeatures.count())
//
//
//    send(rownum.toInt)
//  }
//
//  def send(rownum: Int): Unit = {
//    println("开始发送：" + rownum)
//    AvroProducer2.syncSend(rownum)
//    var conn: Connection = null
//    var ps: PreparedStatement = null
//    try {
//      conn = MysqlOperation.getConnect
//    }
//    try {
//      ps = conn.prepareStatement("update rownumber set rownumber=? where id=1")
//      ps.setDouble(1, rownum + 1441)
//      ps.executeUpdate()
//      println("发送完成：" + rownum)
//    }
//    MysqlOperation.closeConnect(conn, ps, null)
//  }
//}
