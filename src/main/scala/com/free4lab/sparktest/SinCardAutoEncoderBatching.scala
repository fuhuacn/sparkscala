package com.free4lab.sparktest

import java.io.{File, FileWriter}
import java.sql.{Connection, DriverManager, PreparedStatement}

import com.free4lab.sparktest.constants.Con
import com.free4lab.sparktest.operation.{MysqlOperation, WriteHDFS}
import kafka.AvroProducer2
import org.apache.spark.api.java.JavaRDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.DoubleType
import org.deeplearning4j.nn.api.OptimizationAlgorithm
import org.deeplearning4j.nn.conf.NeuralNetConfiguration
import org.deeplearning4j.nn.conf.layers.{AutoEncoder, OutputLayer}
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.nn.weights.WeightInit
import org.deeplearning4j.optimize.listeners.ScoreIterationListener
import org.deeplearning4j.spark.impl.multilayer.SparkDl4jMultiLayer
import org.deeplearning4j.spark.impl.paramavg.ParameterAveragingTrainingMaster
import org.nd4j.evaluation.regression.RegressionEvaluation
import org.nd4j.linalg.activations.Activation
import org.nd4j.linalg.dataset.DataSet
import org.nd4j.linalg.factory.Nd4j
import org.nd4j.linalg.learning.config.Adam
import org.nd4j.linalg.lossfunctions.LossFunctions

/**
 * @Author: fuhua
 * @Date: 2019-06-12 16:17
 *        处理完后发数据
 */
object SinCardAutoEncoderBatching {
  def main(args: Array[String]): Unit = {
    /*
     * 以下是批处理
     */
//        val fileName = "from0test.csv"
    val fileName = "cardguiyihua.csv"

    val header = ",V1,V2,V3,V4,V5,V6,V7,V8,V9,V10,V11,V12,V13,V14,V15,V16,V17,V18,V19,V20,V21,V22,V23,V24,V25,V26,V27,V28,Amount,Class\n"
    val attrLength = header.split(",").length - 2
//        val spark = SparkSession.builder().master("spark://ambari-namenode.com:7077").appName("sinBatching").getOrCreate()
    val spark = SparkSession.builder().master("local[2]").appName("test").getOrCreate()

    val rownum = MysqlOperation.readValue("rownumber", "rownumber", "id=1")
    val unit = MysqlOperation.readValue("rownumber", "rownumber", "id=2")

    import spark.implicits._
    val datasetReadCsv = spark.read.format("csv").option("header", "true").load(Con.FILE_PATH + fileName)

    val datasetOr = datasetReadCsv.orderBy($"_c0".cast(DoubleType).desc).limit((unit + 1).toInt).orderBy($"_c0".cast(DoubleType).asc).map(_.toSeq.foldLeft("")(_ + "," + _).substring(1))
    datasetOr.show()


    datasetOr.createOrReplaceTempView("dataset")
    val datasetResult = spark.sql("select dataset.value as features from dataset")
    println(datasetResult.count())

//    if (datasetResult.count() < unit) {
//      send(rownum.toInt)
//      return
//    }

    val rddResult: JavaRDD[DataSet] = datasetResult.rdd.map(row => {
      val featuresOr = row(0).asInstanceOf[String].split(",")
      val features = Nd4j.create(1, featuresOr.length - 2)
      val labels = Nd4j.create(1, featuresOr.length - 2)
      for (i <- 1 until featuresOr.length - 1) {
        features.putScalar(Array(0, i - 1), featuresOr(i).toDouble)
        labels.putScalar(Array(0, i - 1), featuresOr(i).toDouble)
      }
      new DataSet(features, labels)
    })


    val averagingFrequency = 3
    val examplesPerDataSetObject = 1
    val batchSizePerWorker = 50
    val numEpochs = 1

    val hiddenLayer1Size = 16
    val conf = new NeuralNetConfiguration.Builder()
      .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
      .seed(123)
      .l2(0.001)
      .weightInit(WeightInit.XAVIER)
      .updater(new Adam(10e-5))
      .list()
      .layer(0, new AutoEncoder.Builder().nIn(attrLength).nOut(hiddenLayer1Size)
        .activation(Activation.TANH).build())
      .layer(1, new OutputLayer.Builder(LossFunctions.LossFunction.MSE).activation(Activation.TANH)
        .nIn(hiddenLayer1Size).nOut(attrLength).build())
      .build()


    val tm = new ParameterAveragingTrainingMaster.Builder(examplesPerDataSetObject)
      .workerPrefetchNumBatches(2) //Asynchronously prefetch up to 2 batches
      .averagingFrequency(averagingFrequency)
      .batchSizePerWorker(batchSizePerWorker)
      .build()


    val sparkNetwork: SparkDl4jMultiLayer = new SparkDl4jMultiLayer(spark.sparkContext, conf, tm)
    sparkNetwork.setListeners(new ScoreIterationListener(1))


    //    val Array(trainingData,testingData) = rddResult.randomSplit(Array(0.8,0.2))
    for (j <- 0 until numEpochs) {

      //      val net:MultiLayerNetwork = sparkNetwork.fit(trainingData)
      println("开始训练！！！！")
      val net: MultiLayerNetwork = sparkNetwork.fit(rddResult)
      println("训练结束！！！！")
      val mse = sparkNetwork.evaluateRegression(rddResult).asInstanceOf[RegressionEvaluation].averageMeanSquaredError()
      print("MSE: " + mse)

      var conn: Connection = null
      var ps: PreparedStatement = null
      val sql = "update wssd set wssd=? where id=1"
      try {
        conn = DriverManager.getConnection("jdbc:mysql://daas.free4inno.com:6033/sparkml", "root", "MYSQL@free4inno")
        ps = conn.prepareStatement(sql)
        ps.setDouble(1, mse)
        ps.executeUpdate()
      }

      val fileCrc: File = new File(Con.TEMP_PATH + "." + Con.AUTOENCODER_NAME + ".crc")
      if (fileCrc.exists()) {
        System.out.println("删除crc文件！")
        fileCrc.delete()
      }
      val file: File = new File(Con.TEMP_PATH + Con.AUTOENCODER_NAME)
      net.save(file, true)
//      WriteHDFS.saveFile(Con.TEMP_PATH + Con.AUTOENCODER_NAME, Con.AUTOENCODER_NAME)
      //            val trainEvaluation:Evaluation = sparkNetwork.evaluate(trainingData)
      //      val trainAccuracy = trainEvaluation.accuracy()
      //      val testEvaluation:Evaluation = sparkNetwork.evaluate(testingData)
      //      val testAccuracy = testEvaluation.accuracy()
      //      println(trainAccuracy+" "+testAccuracy)
      println("train finish")

      //
//      val temp2 = "284799,-1.7751349634023703,-0.00423539870249681,1.18978624706472,0.33109591586509,1.19606256869803,5.519979726705031,-1.5181849275299701,2.08082523601415,1.15949814044899,-0.594241721001303,-1.26407181940184,0.45359626684755294,-0.24314249047187897,-0.858718973845223,-0.7665538542329681,-0.644646088590239,0.44718384787284,0.388720697120586,0.792134662376058,0.348175534120535,0.10330163997530802,0.654849731110724,-0.34892909050456106,0.745322962967185,0.704545250649244,-0.12757869782705,0.45437915302798704,0.13030767892758902,0.9053802458708026,0"
//      val temp2s = temp2.split(",")
//      val predictValue2 = Nd4j.create(1, temp2s.length - 2)
//      for (i <- 1 until temp2s.length - 1) {
//        predictValue2.putScalar(Array(0, i - 1), temp2s(i).toDouble)
//      }
//      println()
//
//      val output2 = net.output(predictValue2)
//      var sum:Double = 0.0
//
//      for (i <- 1 until temp2s.length - 1) {
//        sum+=Math.pow((output2.getFloat(Array(1, i - 1)) - temp2s(i).toDouble),2)
//        print(output2.getFloat(Array(1, i - 1)) + " ")
//      }
//      val out = new FileWriter("/Users/fuhua/Desktop/sum.out",true)
//      out.write(s"$sum\n\n")
//      out.close()

    }

//    send(rownum.toInt)
  }

  def send(rownum: Int): Unit = {
        println("开始发送：" + rownum)
        AvroProducer2.syncSend(rownum)
        var conn: Connection = null
        var ps: PreparedStatement = null
        try {
          conn = MysqlOperation.getConnect
        }
        try {
          ps = conn.prepareStatement("update rownumber set rownumber=? where id=1")
          ps.setDouble(1, rownum + 1441)
          ps.executeUpdate()
          println("发送完成：" + rownum)
        }
        MysqlOperation.closeConnect(conn, ps, null)
  }
}
