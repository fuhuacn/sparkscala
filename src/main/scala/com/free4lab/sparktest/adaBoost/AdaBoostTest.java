package com.free4lab.sparktest.adaBoost;

import libsvm.svm_node;
import smile.classification.AdaBoost;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author: fuhua
 * @Date: 2020/9/10 10:40 下午
 */
public class AdaBoostTest {
    public static void main(String[] args) throws Exception {

        List<List<Double>> datas =
                new ArrayList<List<Double>>();
        List<Integer> labels = new ArrayList<Integer>();

        BufferedReader lstm = new BufferedReader(new InputStreamReader(new FileInputStream("/Users/fuhua/Desktop/kmeans2.out")));
        BufferedReader kmeans = new BufferedReader(new InputStreamReader(new FileInputStream("/Users/fuhua/Desktop/kmeans3.out")));
        String lstmValue;
        String kmeansValue;
        int line = 0;
        // normal
        while (line++ < 94728) {
            lstmValue = lstm.readLine();
            kmeansValue = kmeans.readLine();
            addData(datas, labels, lstmValue, kmeansValue, true);
        }
        // abnormal
        while ((lstmValue = lstm.readLine()) != null && (kmeansValue = kmeans.readLine()) != null) {
            addData(datas, labels, lstmValue, kmeansValue, false);
        }


        //转换label
        int[] label = new int[labels.size()];
        for (int i = 0; i < label.length; i++) {
            label[i] = labels.get(i);
        }

        //转换属性
        int rows = datas.size();
        int cols = datas.get(0).size();
        double[][] srcData = new double[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                srcData[i][j] = datas.get(i).get(j);
            }
        }


        AdaBoost adaBoost =
                new AdaBoost(srcData, label, 4, 8);
        double[][] test = new double[1][2];
        test[0][0] = 9.9;
        test[0][1] = 9.9;
        System.out.println(adaBoost.predict(test[0]));

        double right = 0;
        for (int i = 0; i < srcData.length; i++) {

            int tag = adaBoost.predict(srcData[i]);
            if (i % 10 == 0) System.out.println();
            System.out.print(tag + " ");
            if (tag == label[i]) {
                right += 1;
            }
        }
        right = right / srcData.length;
        System.out.println("Accrurate: " + right * 100 + "%");
    }

    private static void addData(List<List<Double>> dataSet, List<Integer> labels, String lstmValue, String kmeansValue, boolean normal) {
        double lstmDouble = Double.parseDouble(lstmValue);
        double kmeansDouble = Double.parseDouble(kmeansValue);
        List<Double> data = new ArrayList<>();
        data.add(lstmDouble);
        data.add(kmeansDouble);
        labels.add(normal ? 1 : 0);
        dataSet.add(data);
    }
}
