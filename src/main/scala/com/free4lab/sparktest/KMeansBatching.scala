package com.free4lab.sparktest

import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.udf

/**
  * @Author: fuhua
  * @Date: 2019-06-12 16:17
  */
object KMeansBatching {
  def main(args: Array[String]): Unit = {

    /*
     * 以下是批处理
     */

    val spark = SparkSession.builder().master("local").appName("test").getOrCreate()
    val toDouble = udf[Double, String](_.toDouble)
    val datasetOr = spark.read.format("csv").option("header", "true").load("/Users/fuhua/Documents/运营商用户行为分析/产出结果/gerenjilu2.csv")

    val dataset = datasetOr.withColumn("社交", toDouble(datasetOr("社交"))).withColumn("阅读", toDouble(datasetOr("阅读")))
    dataset.printSchema()

    val dataWithFeatures = new VectorAssembler()
      .setInputCols(Array("社交", "阅读"))
      .setOutputCol("features")
      .transform(dataset)

    val kmeans = new KMeans().setK(5).setSeed(1L).setPredictionCol("label")
    val model = kmeans.fit(dataWithFeatures)
    val b = model.transform(dataWithFeatures)
    println(b.collect().toBuffer)

    model.save("/Users/fuhua/Desktop/kmeans")

    //Evaluate clustering by computing Within Set Sum of Squared Errors.
//
////    val WSSSE = model.computeCost(dataWithFeaturesTest)
////    println(s"Within Set Sum of Squared Errors = $WSSSE")
//
//    //Shows the result.
    println("Cluster Centers: ")
    model.clusterCenters.foreach(println)

  }
}
