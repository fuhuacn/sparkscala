package com.free4lab.sparktest

import java.sql.{Connection, DriverManager, PreparedStatement}

import com.free4lab.sparktest.operation.MysqlOperation
import org.apache.avro.generic.GenericRecord
import org.apache.avro.util.Utf8
import org.apache.hadoop.mapred.InvalidInputException
import org.apache.spark.ml.clustering.KMeansModel
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.{HasOffsetRanges, KafkaUtils, OffsetRange}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, TaskContext}

/**
  * @Author: fuhua
  * @Date: 2019-06-13 11:57
  * 告警大于8倍基准值，会存到mysql
  */
object TestStreamingCard extends Serializable {


  def main(args: Array[String]): Unit = {
    /*
     * 以下是流处理
     */
    val pattern = ","
    val conf = new SparkConf().setMaster("spark://ambari-namenode.com:7077").setAppName("Streaming")
//        val conf = new SparkConf().setMaster("local").setAppName("Streaming")
    val ssc = new StreamingContext(conf, Seconds(2))


    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> "192.168.34.60:9092,192.168.34.62:9092,192.168.34.57:9092,192.168.34.79:9092,192.168.34.80:9092",
      //      "key.deserializer" -> classOf[StringDeserializer],
      //      "value.deserializer" -> classOf[StringDeserializer],
      "key.deserializer" -> classOf[io.confluent.kafka.serializers.KafkaAvroDeserializer],
      "value.deserializer" -> classOf[io.confluent.kafka.serializers.KafkaAvroDeserializer],
      "schema.registry.url" -> "http://192.168.34.62:8081",
      "group.id" -> "streamTest",
      "auto.offset.reset" -> "latest",
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )

    val topics = Array("SparkStreaming-test-temp")

    val stream = KafkaUtils.createDirectStream[String, String](
      ssc,
      PreferConsistent,
      Subscribe[String, String](topics, kafkaParams)
    )

    val spark = SparkSession
      .builder
      .appName("SparkStreaming-test-temp")
      .getOrCreate()

    /*
1    * 管理offset
     */
    stream.foreachRDD { rdd =>
      val offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
      rdd.foreachPartition { iter =>
        val o: OffsetRange = offsetRanges(TaskContext.get.partitionId)
        println(s"${o.topic} ${o.partition} ${o.fromOffset} ${o.untilOffset}")
      }
    }
    import org.apache.spark.sql.functions._
    import spark.implicits._

    /*
     * 存储至MySQL
     */
    stream.map(p => {
      val unitKafka: UnitKafka = UnitKafka(p.value().asInstanceOf[GenericRecord])
      (unitKafka.attrs, unitKafka.time, 0, unitKafka.index, p.topic())
    }).foreachRDD(rdd => {
      if (!rdd.isEmpty()) {
        val df = rdd.toDF("data", "tag", "time", "index", "topic")
        MysqlOperation.writeDF(df, "data", SaveMode.Append)
        println("save!")
      }
    })

    /*
     *  预警
     */
    stream.map(p => UnitKafka(p.value().asInstanceOf[GenericRecord]).attrs).foreachRDD(rdd => {
      if (!rdd.isEmpty()) {
        println(TaskContext.getPartitionId())//TODO:拿到当前分区号
        val sameModel: KMeansModel = loadModel()

        if (sameModel != null) {

          val wssd: Double = MysqlOperation.readValue("wssd","wssd","id=1")

          val length = rdd.first().split(pattern).length
          val datasetOr = rdd.toDF()
          val columns = new Array[String](length)
          val columnsFeature = new Array[String](length - 2)
          for (i <- 0 until length) {
            columns(i) = i.toString
            if (i > 0 && i<length-1) columnsFeature(i - 1) = i.toString
          }
          //      按指定分隔符拆分value列，生成splitCols列
          var datasetOr2 = datasetOr.withColumn("splitCols", split($"value", pattern))
          columns.zipWithIndex.foreach(x => {
            datasetOr2 = datasetOr2.withColumn(String.valueOf(x._1), $"splitCols".getItem(x._2))
          })
          val toDouble = udf[Double, String](_.toDouble)
          var dataset = datasetOr2
          for (column <- columnsFeature) {
            dataset = dataset.withColumn(String.valueOf(column), toDouble(datasetOr2(String.valueOf(column))))
          }
          val dataWithFeaturesOr = new VectorAssembler()
            .setInputCols(columnsFeature)
            .setOutputCol("features")
            .transform(dataset)
          val dataWithFeatures = dataWithFeaturesOr.drop("value", "splitCols")
          val out = sameModel.transform(dataWithFeatures)
          val cluster = sameModel.clusterCenters

          out.foreachPartition(p=>{
            var conn: Connection = null
            var ps: PreparedStatement = null
            p.foreach(row=>{
              val eachSeq = row.toSeq
              val label = eachSeq(length + 1).asInstanceOf[Int]
              val labelValue: Array[Double] = cluster(label).toArray
              var sum: Double = 0
              for (i <- 1 until length-1) {
                val attr = eachSeq(i).asInstanceOf[Double] - labelValue(i - 1)
                sum += attr * attr
              }
              println(s"Within Set Sum of Squared Errors = $sum")
              if (sum > 4*wssd) {
                println("alarm!!!!")
                val sql = "insert into alarm(`index`,`time`,`modelvalue`) values (?,?,?)"
                try {
                  conn = DriverManager.getConnection("jdbc:mysql://daas.free4inno.com:6033/sparkml", "root", "MYSQL@free4inno")
                  ps = conn.prepareStatement(sql)
                  ps.setInt(1, eachSeq(0).asInstanceOf[String].toInt)
                  ps.setLong(2, 0)
                  ps.setDouble(3, sum)
                  ps.executeUpdate()
                } catch {
                  case e: Exception => e.printStackTrace()
                }
              }
            })
            MysqlOperation.closeConnect(conn,ps,null)
          })
        }
      }
    })

    ssc.start()
    ssc.awaitTermination()

    def loadModel(): KMeansModel ={
      var sameModel:KMeansModel = null
      try {
//        sameModel = KMeansModel.load("/Users/fuhua/Desktop/kmeans-model").setPredictionCol("label")
        sameModel = KMeansModel.load("hdfs://ambari-namenode.com:8020/sintest/kmeans-model").setPredictionCol("label")
      } catch {
        case e: InvalidInputException => {
          println("无训练记录")
        }
        case e: UnsupportedOperationException => {
          println("上次结果仍没有完成！")
          Thread.sleep(10000)
          sameModel = loadModel()
        }
      }
      sameModel
    }

    case class UnitKafka(gen: GenericRecord) {
      val propsJava: java.util.Map[String, String] = gen.get("props").asInstanceOf[java.util.Map[String, String]]
      var attrs = ""
      var time = "" //在KDD这假设为标签
      var index = ""
      for (i <- 0 until propsJava.size()) {
        var mapKey: Utf8 = new Utf8(String.valueOf(i))
        val attrEach = propsJava.get(mapKey).asInstanceOf[Utf8].toString
        attrs += attrEach + pattern
        if (i == 0) {
          index = attrEach
        }
        if(i==propsJava.size()-1){
          if(attrEach.equals("0")){
            time = "1"
          }else{
            time = "-1"
          }
        }
      }
      attrs = attrs.substring(0, attrs.length - 1)
      val id_attrs = (index, attrs)
    }


  }
}
