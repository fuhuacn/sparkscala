package com.free4lab.sparktest

import java.io.File

import com.free4lab.sparktest.constants.Con
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.nd4j.linalg.factory.Nd4j

/**
 * @Author: fuhua
 * @Date: 2019-06-12 16:17
 *        处理完后发数据
 */
object TestLSTMBatching {
  def main(args: Array[String]): Unit = {
    val file: File = new File(Con.FILE_PATH + Con.LSTM_FILE_NAME)
    val net = MultiLayerNetwork.load(file, false)

    val temp = "7619,1383692340000,110.34394,171.71593,126.20419,107.20651,114.23695,108.49582,122.76592,132.98669,102.86299,140.84427,103.84548,101.24248,137.42106,109.76693,134.84385,116.41904,107.31220,148.95088,98.55378,135.80619,133.56615,119.54032,138.57895"
    val temp2 = "0,1383235200000,108.25327,155.62151,130.46129,107.07230,122.66426,118.41837,129.12730,138.72046,101.00295,141.77298,103.29820,100.06393,133.39351,107.90030,131.23010,129.49349,106.45130,157.59255,99.04134,134.20338,133.80026,108.66940,135.20425"
    val predictValue = Nd4j.zeros(1, 23, 1)

    val temps = temp.split(",")
    for (i <- 2 until temps.length) {
      predictValue.putScalar(Array(1, i - 2, 1), temps(i).toDouble)
    }
    //      val output:INDArray = net.rnnTimeStep(predictValue)
    val output = net.output(predictValue)
    for (i <- 2 until temps.length) {
      print(output.getFloat(1, i - 2, 1) + " ")
    }

    val predictValue2 = Nd4j.zeros(1, 23, 1)
    val temp2s = temp2.split(",")
    for (i <- 2 until temp2s.length) {
      predictValue2.putScalar(Array(1, i - 2, 1), temp2s(i).toDouble)
    }
    println()
    //
    //
    //      val output:INDArray = net.rnnTimeStep(predictValue)
    val output2 = net.output(predictValue2)
    for (i <- 2 until temps.length) {
      print(output2.getFloat(1, i - 2, 1) + " ")
    }

    //    val dataWithFeatures = new VectorAssembler()
    //      .setInputCols(featrueNames)
    //      .setOutputCol("features")
    //      .transform(dataset)
    //
    //    println(dataWithFeatures.count())


    //    send(rownum)
  }
}
