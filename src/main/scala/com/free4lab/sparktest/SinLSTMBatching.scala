package com.free4lab.sparktest

import java.io.{File, FileWriter}
import java.sql.{Connection, DriverManager, PreparedStatement, ResultSet, Statement}
import java.util.Random

import com.free4lab.sparktest.constants.{CommonMethods, Con}
import com.free4lab.sparktest.operation.WriteHDFS
import kafka.AvroProducer2
import org.apache.spark.api.java.JavaRDD
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.deeplearning4j.nn.api.OptimizationAlgorithm
import org.deeplearning4j.nn.conf.layers.{AutoEncoder, GravesLSTM, LSTM, LearnedSelfAttentionLayer, RnnOutputLayer}
import org.deeplearning4j.nn.conf.{BackpropType, GradientNormalization, MultiLayerConfiguration, NeuralNetConfiguration}
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.nn.weights.WeightInit
import org.deeplearning4j.optimize.listeners.ScoreIterationListener
import org.deeplearning4j.spark.impl.multilayer.SparkDl4jMultiLayer
import org.deeplearning4j.spark.impl.paramavg.ParameterAveragingTrainingMaster
import org.nd4j.linalg.activations.Activation
import org.nd4j.linalg.dataset.DataSet
import org.nd4j.linalg.factory.Nd4j
import org.nd4j.linalg.learning.config.{Adam, IUpdater, Nadam, Nesterovs, RmsProp}
import org.nd4j.linalg.lossfunctions.LossFunctions.LossFunction
import operation.MysqlOperation
import org.nd4j.evaluation.regression.RegressionEvaluation
import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.lossfunctions.LossFunctions

import scala.collection.mutable

/**
 * @Author: fuhua
 * @Date: 2019-06-12 16:17
 */
object SinLSTMBatching {
  def main(args: Array[String]): Unit = {
    func(10080)
  }

  def func(unit:Int): Double ={
    /*
     * 以下是批处理
     */
    //    val fileName = "from0test.csv"
    val fileName = "2.csv"

    val header = ",time,循环氢压缩机出口压力,系统压降,E105高压空冷管程介质入口温度,循环氢压缩机入口压力,E102高压换热器管程入口温度（E101出口）,E101高压换热器壳程入口温度,E201换热器管程出口温度,E102高压换热器管程出口温度,分馏重沸炉入口温度（塔底温度）,E102高压换热器壳程入口温度,分馏重沸炉对流室出口温度,精制反应器入口温度,E201换热器管程入口温度,精制反应器入口压力,E201换热器壳程出口温度,E201换热器壳程入口温度（E102壳出口）,分馏重沸炉排烟温度,E210空冷管程介质入口温度,精制反应器床层总压降,E101高压换热器壳程出口温度,精制反应器平均温度,混氢瞬时流量,冷低分压力\n"
    val attrLength = header.split(",").length - 2
    //    val spark = SparkSession.builder().master("spark://ambari-namenode.com:7077").appName("sinBatching").getOrCreate()
    val spark = SparkSession.builder().master("local[2]").appName("test").getOrCreate()

    //    val unit = MysqlOperation.readValue("rownumber", "rownumber", "id=2")

    import spark.implicits._
    val datasetReadCsv = spark.read.format("csv").option("header", "true").load("/Users/fuhua/Documents/自动监测错误系统/代码要用的内容/2.csv")

    val datasetRead = datasetReadCsv.orderBy($"_c0".cast(IntegerType).asc).limit(unit + 1).map(_.toSeq.foldLeft("")(_ + "," + _).substring(1))

    import org.apache.spark.sql.functions._
    val datasetOr = datasetRead.withColumn("id", monotonically_increasing_id())

    val datasetResult = lstm5to1(datasetOr,spark)
    datasetResult.show()

    //    if (datasetRead.count() < unit) {
    //      send(rownum.toInt)
    //      return
    //    }
    val noTrainSets: mutable.Set[Int] = mutable.Set[Int]()
    noTrainSets.add(0)
    noTrainSets.add(1)
    val rddResult: JavaRDD[DataSet] = datasetResult.rdd.map(row => {
      val eachFeatures = row.getString(0).split(";")
      val features = Nd4j.create(1, 23, eachFeatures.length)
      val labels = Nd4j.zeros(1, 23,eachFeatures.length)
      val featuresMask = Nd4j.create(1, eachFeatures.length)
      val labelsMask = Nd4j.create(1, eachFeatures.length)

      val labelsOr = row(1).asInstanceOf[String].split(",")
      for(j <- 0 until eachFeatures.length){
        val eachFeature = eachFeatures(j)
        val featuresOr = eachFeature.asInstanceOf[String].split(",")

        var labelIndex = 0
        for (i <- 0 until featuresOr.length) {
          if (!noTrainSets.contains(i)) {
            features.putScalar(Array(0, labelIndex, j), featuresOr(i).toDouble)
            featuresMask.putScalar(Array(0, j), 1)
            if(j==0){
              labels.putScalar(Array(0, labelIndex, 0), labelsOr(i).toDouble)
              labelsMask.putScalar(Array(0, 0), 1)
            }
            labelIndex += 1
          }
        }
      }
      new DataSet(features, labels, featuresMask, labelsMask)
    })

    val tbpttLength = 50 //Length for truncated backpropagation through time. i.e., do parameter updates ever 50 characters
    val averagingFrequency = 3
    val examplesPerDataSetObject = 1
    val batchSizePerWorker = 50
    val numEpochs = 1

    val lstmLayer1Size = 100
    val encodeSize = 10
    val lstmLayer2Size = 100
    val conf = new NeuralNetConfiguration.Builder()
      .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
      .seed(123)
      .l2(0.0006)
      .weightInit(WeightInit.XAVIER)
      .updater(new Adam(0.001))
      .list()
      .layer(0, new LSTM.Builder().nIn(attrLength).nOut(lstmLayer1Size)
        .activation(Activation.IDENTITY).build())
      .layer(1, new AutoEncoder.Builder().nIn(lstmLayer1Size).nOut(encodeSize)
        .activation(Activation.IDENTITY).build())
      .layer(2, new LSTM.Builder().nIn(encodeSize).nOut(lstmLayer2Size)
        .activation(Activation.IDENTITY).build())
      .layer(3, new RnnOutputLayer.Builder(LossFunctions.LossFunction.MSE).activation(Activation.IDENTITY)
        .nIn(lstmLayer2Size).nOut(attrLength).build())
      .build()


    val tm = new ParameterAveragingTrainingMaster.Builder(examplesPerDataSetObject)
      .workerPrefetchNumBatches(2) //Asynchronously prefetch up to 2 batches
      .averagingFrequency(averagingFrequency)
      .batchSizePerWorker(batchSizePerWorker)
      .build()

    val keepTraining = true//TODO:为true则会找之前模型继续训练，rownumber数量将是每次取的数量
    var historyNet:MultiLayerNetwork = null
    if(keepTraining){
      historyNet = CommonMethods.loadLSTMModel(Con.LSTM_FILE_NAME)
    }
    var sparkNetwork:SparkDl4jMultiLayer = null
    if(historyNet!=null){
      println("将使用之前模型继续训练！")
      sparkNetwork = new SparkDl4jMultiLayer(spark.sparkContext,historyNet,tm)
    }else{
      println("开始新的训练！")
      sparkNetwork = new SparkDl4jMultiLayer(spark.sparkContext, conf, tm)
    }

    sparkNetwork.setListeners(new ScoreIterationListener(1))

    var mse = 0.0
    //    val Array(trainingData,testingData) = rddResult.randomSplit(Array(0.8,0.2))
    for (j <- 0 until numEpochs) {

      //      val net:MultiLayerNetwork = sparkNetwork.fit(trainingData)
      println("开始训练！！！！")
      val net: MultiLayerNetwork = sparkNetwork.fit(rddResult)
      println("训练结束！！！！")

      //      val fileCrc: File = new File(Con.TEMP_PATH + "." + Con.LSTM_FILE_NAME + ".crc")
      //      if (fileCrc.exists()) {
      //        System.out.println("删除crc文件！")
      //        fileCrc.delete()
      //      }
      val file: File = new File(Con.TEMP_PATH + Con.LSTM_FILE_NAME)
      net.save(file, true)
      //      WriteHDFS.saveFile(Con.TEMP_PATH + Con.LSTM_FILE_NAME, Con.LSTM_FILE_NAME)
      //            val trainEvaluation:Evaluation = sparkNetwork.evaluate(trainingData)
      //      val trainAccuracy = trainEvaluation.accuracy()
      //      val testEvaluation:Evaluation = sparkNetwork.evaluate(testingData)
      //      val testAccuracy = testEvaluation.accuracy()
      //      println(trainAccuracy+" "+testAccuracy)
      println("train finish")
      mse = sparkNetwork.evaluateRegression(rddResult).asInstanceOf[RegressionEvaluation].averageMeanSquaredError()

//      val out2 = new FileWriter("/Users/fuhua/Desktop/kmeans2.out",true)
//      out2.write(s"fuhua = $mse\n")
//      out2.close()
      //
      //      val predictValue2 = Nd4j.zeros(1, 23, 1)
      //      val temp2 = "7619,1383692340000,110.34394,171.71593,126.20419,107.20651,114.23695,108.49582,122.76592,132.98669,102.86299,140.84427,103.84548,101.24248,137.42106,109.76693,134.84385,116.41904,107.31220,148.95088,98.55378,135.80619,133.56615,119.54032,138.57895"
      //      val temp2s = temp2.split(",")
      //      for (i <- 2 until temp2s.length) {
      //        predictValue2.putScalar(Array(1, i - 2, 1), temp2s(i).toDouble)
      //      }
      //      println()
      //
      //
      //      val output2 = net.output(predictValue2)
      //      for (i <- 2 until 25) {
      //        print(output2.getFloat(1, i - 2, 1) + " ")
      //      }
    }

    //    val dataWithFeatures = new VectorAssembler()
    //      .setInputCols(featrueNames)
    //      .setOutputCol("features")
    //      .transform(dataset)
    //
    //    println(dataWithFeatures.count())


    //    send(rownum.toInt)
    Math.pow(mse, 0.5)
  }

  def send(rownum: Int): Unit = {
    println("开始发送：" + rownum)
    AvroProducer2.syncSend(rownum)
    var conn: Connection = null
    var ps: PreparedStatement = null
    try {
      conn = MysqlOperation.getConnect
    }
    try {
      ps = conn.prepareStatement("update rownumber set rownumber=? where id=1")
      ps.setDouble(1, rownum + 1441)
      ps.executeUpdate()
      println("发送完成：" + rownum)
    }
    MysqlOperation.closeConnect(conn, ps, null)
  }

  def lstm5to1(datasetOr:DataFrame, spark: SparkSession): DataFrame ={
    import org.apache.spark.sql.functions._
    import spark.implicits._
    val datasetOr2 =  datasetOr.as("d1").join(datasetOr.as("d2"),$"d1.id"===$"d2.id"-1).join(datasetOr.as("d3"),$"d1.id"===$"d3.id"-2).join(datasetOr.as("d4"),$"d1.id"===$"d4.id"-3).join(datasetOr.as("d5"),$"d1.id"===$"d5.id"-4).join(datasetOr.as("d6"),$"d1.id"===$"d6.id"-5).select(concat_ws(";",$"d1.value" as "v1",$"d2.value" as "v2",$"d3.value" as "v3",$"d4.value" as "v4",$"d5.value" as "v5") as "features" ,$"d6.value" as "label")
    datasetOr2
  }
}
