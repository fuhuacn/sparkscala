package com.free4lab.sparktest

import java.sql.{Connection, DriverManager, PreparedStatement, ResultSet, Statement}

import com.free4lab.sparktest.operation.WriteHDFS
import kafka.AvroProducer2
import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.DoubleType

/**
  * @Author: fuhua
  * @Date: 2019-06-12 16:17
  *        窗口滑动处理
  */
object SinBatching3 {
  def func(num:Int): Double = {
    /*
     * 以下是批处理
     */
    val header = ",time,循环氢压缩机出口压力,系统压降,E105高压空冷管程介质入口温度,循环氢压缩机入口压力,E102高压换热器管程入口温度（E101出口）,E101高压换热器壳程入口温度,E201换热器管程出口温度,E102高压换热器管程出口温度,分馏重沸炉入口温度（塔底温度）,E102高压换热器壳程入口温度,分馏重沸炉对流室出口温度,精制反应器入口温度,E201换热器管程入口温度,精制反应器入口压力,E201换热器壳程出口温度,E201换热器壳程入口温度（E102壳出口）,分馏重沸炉排烟温度,E210空冷管程介质入口温度,精制反应器床层总压降,E101高压换热器壳程出口温度,精制反应器平均温度,混氢瞬时流量,冷低分压力\n"
//    val spark = SparkSession.builder().master("spark://ambari-namenode.com:7077").appName("sinBatching").getOrCreate()
        val spark = SparkSession.builder().master("local[2]").appName("test").getOrCreate()

//    WriteHDFS.createFile(fileName, header)

//    val datasetOrOrgin = spark.read.format("csv").option("header", "true").load(s"hdfs://ambari-namenode.com:8020/sintest/$fileName")
        val datasetOrOrgin = spark.read.format("csv").option("header", "true").load("/Users/fuhua/Documents/自动监测错误系统/代码要用的内容/2.csv")

//    var rownum = 0
//    var conn: Connection = null
//    var state: Statement = null
//    var ps: PreparedStatement = null
//    try {
//      conn = DriverManager.getConnection("jdbc:mysql://daas.free4inno.com:6033/sparkml", "root", "MYSQL@free4inno")
//      state = conn.createStatement()
//      val rs: ResultSet = state.executeQuery("select rownumber from rownumber where id =1")
//      rs.next()
//      rownum = rs.getInt(1)
//      rs.close()
//    } catch {
//      case e: Exception => e.printStackTrace()
//    }
//    if (ps != null) {
//      ps.close()
//    }
//    if (conn != null) {
//      conn.close()
//    }


//    var unit = 1440*7 //一天的数据
//    try {
//      conn = DriverManager.getConnection("jdbc:mysql://daas.free4inno.com:6033/sparkml", "root", "MYSQL@free4inno")
//      state = conn.createStatement()
//      val rs: ResultSet = state.executeQuery("select rownumber from rownumber where id =2")
//      rs.next()
//      unit = rs.getInt(1)
//      rs.close()
//    } catch {
//      case e: Exception => e.printStackTrace()
//    }
//    if (ps != null) {
//      ps.close()
//    }
//    if (conn != null) {
//      conn.close()
//    }

    import spark.implicits._

    val datasetOr = datasetOrOrgin.orderBy($"_c0".asc).limit(10080)
    //    val datasetOr = datasetOrOrgin.orderBy($"_c0".desc).filter($"_c0" <= rownum && $"_c0" >= rownum - unit)//TODO:时间限制

    /*
     * 选最近的时间窗口
     */
    val datasetNumber = datasetOr.count()
    println(s"datasetNumber is: $datasetNumber")
//    if (datasetNumber < unit) {
//      send(rownum)
//      return
//    }

    val colNames = datasetOr.columns
    val featrueNames = new Array[String](colNames.length - 2)
    var dataset = datasetOr
    for (i <- 0 until colNames.length) {
      dataset = dataset.withColumn(colNames(i), col(colNames(i)).cast(DoubleType))
      if (i != 0 && i != 1) {
        featrueNames(i - 2) = colNames(i)
      }
    }
    dataset.printSchema()
    print(featrueNames.toBuffer)
    val dataWithFeatures = new VectorAssembler()
      .setInputCols(featrueNames)
      .setOutputCol("features")
      .transform(dataset)

//    println("unit:"+unit)
    println(dataWithFeatures.count())
    var WSSSE = 0.0
    for (i <- 30 to 30) {
      val kmeans = new KMeans().setK(i).setSeed(1L).setPredictionCol("label")
      val model = kmeans.fit(dataWithFeatures)
//      model.write.overwrite().save("hdfs://ambari-namenode.com:8020/sintest/kmeans-model")
            model.write.overwrite().save("/Users/fuhua/Desktop/kmeans-model")

      //Evaluate clustering by computing Within Set Sum of Squared Errors.
      //
      WSSSE = model.computeCost(dataWithFeatures) / dataWithFeatures.count()

      println(s"$i\nWithin Set Sum of Squared Errors = $WSSSE\n\n")
//      var conn: Connection = null
//      var ps: PreparedStatement = null
//      val sql = "update wssd set wssd=? where id=1"
//      try {
//        conn = DriverManager.getConnection("jdbc:mysql://daas.free4inno.com:6033/sparkml", "root", "MYSQL@free4inno")
//        ps = conn.prepareStatement(sql)
//        ps.setDouble(1, WSSSE)
//        ps.executeUpdate()
//      }
    }
//    send(rownum)
    WSSSE
  }

  def send(rownum: Int): Unit = {
    var conn: Connection = null
    var ps: PreparedStatement = null
    try {
      conn = DriverManager.getConnection("jdbc:mysql://daas.free4inno.com:6033/sparkml", "root", "MYSQL@free4inno")
    }
    println("开始发送：" + rownum)
    AvroProducer2.syncSend(rownum)
    try {
      ps = conn.prepareStatement("update rownumber set rownumber=? where id=1")
      ps.setDouble(1, rownum + 1441)
      ps.executeUpdate()
      println("发送完成：" + rownum)
    }
    if (ps != null) {
      ps.close()
    }
    if (conn != null) {
      conn.close()
    }
  }
}
