package com.free4lab.sparktest

import java.io.FileWriter
import java.sql.{Connection, DriverManager, PreparedStatement, ResultSet, Statement}

import com.free4lab.sparktest.constants.Con
import kafka.AvroProducer2
import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.DoubleType

/**
 * @Author: fuhua
 * @Date: 2019-06-12 16:17
 *        窗口滑动处理
 */
object SinCardBatching {
  def main(args: Array[String]): Unit = {
    /*
     * 以下是批处理
     */
    val fileName = "from0test.csv"
    val spark = SparkSession.builder().master("spark://ambari-namenode.com:7077").appName("sinBatching").getOrCreate()
//  val spark = SparkSession.builder().master("local[2]").appName("test").getOrCreate()

    val datasetOrOrgin = spark.read.format("csv").option("header", "true").load(Con.FILE_PATH+fileName)

    var rownum = 0
    var conn: Connection = null
    var state: Statement = null
    var ps: PreparedStatement = null
    try {
      conn = DriverManager.getConnection("jdbc:mysql://daas.free4inno.com:6033/sparkml", "root", "MYSQL@free4inno")
      state = conn.createStatement()
      val rs: ResultSet = state.executeQuery("select rownumber from rownumber where id =1")
      rs.next()
      rownum = rs.getInt(1)
      rs.close()
    } catch {
      case e: Exception => e.printStackTrace()
    }
    if (ps != null) {
      ps.close()
    }
    if (conn != null) {
      conn.close()
    }


    var unit = 1440*7 //一天的数据
    try {
      conn = DriverManager.getConnection("jdbc:mysql://daas.free4inno.com:6033/sparkml", "root", "MYSQL@free4inno")
      state = conn.createStatement()
      val rs: ResultSet = state.executeQuery("select rownumber from rownumber where id =2")
      rs.next()
      unit = rs.getInt(1)
      rs.close()
    } catch {
      case e: Exception => e.printStackTrace()
    }
    if (ps != null) {
      ps.close()
    }
    if (conn != null) {
      conn.close()
    }

    import spark.implicits._

//    val datasetOr = datasetOrOrgin
    println("rownumer:"+rownum+" "+"unit:"+unit)
//    val datasetOr = datasetOrOrgin.orderBy($"_c0".desc).filter($"_c0" <= rownum && $"_c0" >= rownum - unit)//TODO:时间限制
    val datasetOr = datasetOrOrgin.orderBy($"_c0".desc).limit(unit)//TODO:时间限制

    /*
     * 选最近的时间窗口
     */
    val datasetNumber = datasetOr.count()
    println(s"datasetNumber is: $datasetNumber")
    if (datasetNumber < unit) {
      send(rownum)
      return
    }

    val colNames = datasetOr.columns
    val featrueNames = new Array[String](colNames.length - 2)
    var dataset = datasetOr
    for (i <- 0 until colNames.length-1) {
      dataset = dataset.withColumn(colNames(i), col(colNames(i)).cast(DoubleType))
      if (i != 0) {
        featrueNames(i - 1) = colNames(i)
      }
    }
    dataset.printSchema()
    print(featrueNames.toBuffer)
    val dataWithFeatures = new VectorAssembler()
      .setInputCols(featrueNames)
      .setOutputCol("features")
      .transform(dataset)

    println("unit:"+unit)
    println(dataWithFeatures.count())
    for (i <- 15 to 15) {
      val kmeans = new KMeans().setK(i).setSeed(1L).setPredictionCol("label")
      val model = kmeans.fit(dataWithFeatures)
      model.write.overwrite().save("hdfs://ambari-namenode.com:8020/sintest/kmeans-model")
      //      model.write.overwrite().save("/Users/fuhua/Desktop/kmeans-model")

      //Evaluate clustering by computing Within Set Sum of Squared Errors.
      //
      val WSSSE: Double = model.computeCost(dataWithFeatures) / dataWithFeatures.count()

      println(s"$i\nWithin Set Sum of Squared Errors = $WSSSE\n\n")
//      val out = new FileWriter("/Users/fuhua/Desktop/kmeans-sin2.out",true)
//      out.write(s"$i\nWithin Set Sum of Squared Errors = $WSSSE\n\n")
//      out.close()
      var conn: Connection = null
      var ps: PreparedStatement = null
      val sql = "update wssd set wssd=? where id=1"
      try {
        conn = DriverManager.getConnection("jdbc:mysql://daas.free4inno.com:6033/sparkml", "root", "MYSQL@free4inno")
        ps = conn.prepareStatement(sql)
        ps.setDouble(1, WSSSE)
        ps.executeUpdate()
      }
    }
    send(rownum)
  }

  def send(rownum: Int): Unit = {
    var conn: Connection = null
    var ps: PreparedStatement = null
    try {
      conn = DriverManager.getConnection("jdbc:mysql://daas.free4inno.com:6033/sparkml", "root", "MYSQL@free4inno")
    }
    println("开始发送：" + rownum)
    AvroProducer2.syncSend(rownum)
    try {
      ps = conn.prepareStatement("update rownumber set rownumber=? where id=1")
      ps.setDouble(1, rownum + 1441)
      ps.executeUpdate()
      println("发送完成：" + rownum)
    }
    if (ps != null) {
      ps.close()
    }
    if (conn != null) {
      conn.close()
    }
  }
}
