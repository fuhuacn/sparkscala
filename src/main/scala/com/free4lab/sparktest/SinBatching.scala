package com.free4lab.sparktest

import java.io.FileWriter

import com.free4lab.sparktest.operation.WriteHDFS
import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.DoubleType

/**
  * @Author: fuhua
  * @Date: 2019-06-12 16:17
  */
object SinBatching {
  def main(args: Array[String]): Unit = {
    /*
     * 以下是批处理
     */
    val fileName = "from0test2.csv"
    val header = ",4,5,35,36,41\n"
//    val spark = SparkSession.builder().master("spark://ambari-namenode.com:7077").appName("sinBatching").getOrCreate()
    val spark = SparkSession.builder().master("local[2]").appName("test").getOrCreate()

//    WriteHDFS.createFile(fileName,header)

//    val datasetOr = spark.read.format("csv").option("header", "true").load(s"hdfs://ambari-namenode.com:8020/sintest/$fileName")
    val datasetOrOr = spark.read.format("csv").option("header", "true").load("/Users/fuhua/Desktop/kddres2.csv")
    import spark.implicits._
    val datasetOr = datasetOrOr.orderBy($"_c0".desc).limit(5760)//TODO:时间限制
    datasetOr.show()
    /*
     * TODO://这个地方的10000未来是否要改成让用户自己设定数量？
     */
    val datasetNumber = datasetOr.count()
    if(datasetNumber<=0){
      println(s"datasetNumber is: $datasetNumber")
      return
    }

    val colNames = datasetOr.columns
    val featrueNames = new Array[String](colNames.length - 2)
    var dataset = datasetOr
    for (i <- 0 until colNames.length-1) {
      dataset = dataset.withColumn(colNames(i), col(colNames(i)).cast(DoubleType))
      if (i != 0) {
        featrueNames(i - 1) = colNames(i)
      }
    }
    dataset.printSchema()
    print(featrueNames.toBuffer)
    val dataWithFeatures = new VectorAssembler()
      .setInputCols(featrueNames)
      .setOutputCol("features")
      .transform(dataset)

    println(dataWithFeatures.count())
    for(i <- 10 to 30){
      val kmeans = new KMeans().setK(i).setSeed(1L).setPredictionCol("label")
      val model = kmeans.fit(dataWithFeatures)
//      model.write.overwrite().save("hdfs://ambari-namenode.com:8020/sintest/kmeans-model")
//      model.save("/Users/fuhua/Desktop/kmeans-model")
//      val model = KMeansModel.load("/Users/fuhua/Desktop/kmeans-sin")


      //Evaluate clustering by computing Within Set Sum of Squared Errors.
      //
      val WSSSE: Double = model.computeCost(dataWithFeatures) / dataWithFeatures.count()

      println(s"$i\nWithin Set Sum of Squared Errors = $WSSSE\n\n")

      val res = model.transform(dataWithFeatures)
      res.createOrReplaceTempView("result")

      val sqlDF = spark.sql("SELECT label,count(*) as cou from result group by label order by cou ASC")
      sqlDF.show()

      val out = new FileWriter("/Users/fuhua/Desktop/kmeans-sin2.out",true)
      out.write(s"$i\nWithin Set Sum of Squared Errors = $WSSSE\n\n")
      out.close()
      //
      //    //Shows the result.
      println("Cluster Centers: ")
      model.clusterCenters.foreach(println)
    }

  }

}
