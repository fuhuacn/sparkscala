package com.free4lab.sparktest

import java.io.{File, FileWriter}
import java.sql.{Connection, PreparedStatement}

import com.free4lab.sparktest.constants.{CommonMethods, Con}
import com.free4lab.sparktest.operation.{MysqlOperation, WriteHDFS}
import org.apache.avro.generic.GenericRecord
import org.apache.avro.util.Utf8
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{DataTypes, IntegerType, LongType, StructField, StructType}
import org.apache.spark.sql.{Row, SaveMode, SparkSession}
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.{HasOffsetRanges, KafkaUtils, OffsetRange}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, TaskContext}
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.nd4j.linalg.factory.Nd4j

/**
 * @Author: fuhua
 * @Date: 2019-06-13 11:57
 *        告警大于8倍基准值，会存到mysql
 */
object TestStreamingLSTM extends Serializable {


  def main(args: Array[String]): Unit = {
    /*
     * 以下是流处理
     */
    val pattern = ","
    //    val conf = new SparkConf().setMaster("spark://ambari-namenode.com:7077").setAppName("Streaming")
    val conf = new SparkConf().setMaster("local[2]").setAppName("Streaming")
    val ssc = new StreamingContext(conf, Seconds(60))


    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> "192.168.34.60:9092,192.168.34.62:9092,192.168.34.57:9092,192.168.34.79:9092,192.168.34.80:9092",
      //      "key.deserializer" -> classOf[StringDeserializer],
      //      "value.deserializer" -> classOf[StringDeserializer],
      "key.deserializer" -> classOf[io.confluent.kafka.serializers.KafkaAvroDeserializer],
      "value.deserializer" -> classOf[io.confluent.kafka.serializers.KafkaAvroDeserializer],
      "schema.registry.url" -> "http://192.168.34.62:8081",
      "group.id" -> "streamTest",
      "auto.offset.reset" -> "latest",
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )

    val topics = Array("SparkStreaming-test-temp-lstm200")

    val stream = KafkaUtils.createDirectStream[String, String](
      ssc,
      PreferConsistent,
      Subscribe[String, String](topics, kafkaParams)
    )

    val spark = SparkSession
      .builder
      .appName("SparkStreaming-test-temp")
      .getOrCreate()


    import org.apache.spark.sql.functions._
    import spark.implicits._

    /*
     * 告警
     */
    var limit = 5000

    stream.map(p => UnitKafka(p.value().asInstanceOf[GenericRecord]).attrs)
      .foreachRDD(rdd => {
        if (!rdd.isEmpty()) {

          println("Data is coming!")
          val calMse = SinLSTMBatching.func(limit)
          limit = limit+5000
          var sameModel: MultiLayerNetwork = null
          val file: File = new File(Con.TEMP_PATH + "lstm.zip")
          if (file.exists()) {
            sameModel = MultiLayerNetwork.load(file, true)
          } else {
            println("只有本地文件，hdfs没有文件")
          }

          if (sameModel == null) {
            println("Streaming null model")
          } else {
            println("Begin to check!")
            val df = rdd.toDF("value")
            val df_deal = df.withColumn("index", split($"value", ",")(0)).withColumn("indexNew", $"index".cast(DataTypes.DoubleType)).orderBy($"indexNew".asc).drop("indexNew").drop("index")
            //    val w = Window.orderBy("indexNew")
            //    val datasetOr = df_deal.withColumn("id", row_number().over(w)).drop("indexNew").drop("index")
            val newSchema: StructType = df_deal.schema.add(StructField("id", LongType))
            val dfTempRDD: RDD[(Row, Long)] = df_deal.rdd.zipWithIndex()
            val rowTempRDD: RDD[Row] = dfTempRDD.map(tp => Row.merge(tp._1, Row(tp._2)))
            val datasetOr = spark.createDataFrame(rowTempRDD, newSchema)

            val datasetResult = SinLSTMBatching.lstm5to1(datasetOr, spark)

            datasetResult.show()

            datasetResult.foreach(row => {
              val eachFeatures = row.getString(0).split(";")
              val predictValue = Nd4j.zeros(1, 23, eachFeatures.length)
              for (j <- 0 until eachFeatures.length) {
                val features = eachFeatures(j).split(",")
                var labelIndex = 0
                for (i <- 0 until 25) {
                  if (i != 0 && i != 1) {
                    predictValue.putScalar(Array(0, labelIndex, j), features(i).toDouble)
                    labelIndex += 1
                  }
                }
              }


              val reals = row.getString(1).split(",")

              var labelIndex = 0
              val predict = sameModel.output(predictValue)
              val predictString = new StringBuffer()
              var sum: Double = 0.0
              for (i <- 0 until reals.length) {
                if (i != 0 && i != 1) {
                  val eachV = predict.getFloat(0, labelIndex, 0)
                  sum += Math.pow(eachV - reals(i).toDouble, 2)
                  labelIndex += 1
                  predictString.append(eachV + ",")
                }
              }
              sum = Math.pow(sum, 0.5) / calMse
              val out2 = new FileWriter("/Users/fuhua/Desktop/kmeans2.out", true)
              out2.write(s"$sum\n")
              out2.close()
            })
          }
        }
      })


    ssc.start()
    ssc.awaitTermination()

    case class UnitKafka(gen: GenericRecord) {
      var attrs = ""
      var createdTime = ""

      val propsJava: java.util.Map[String, String] = gen.get("props").asInstanceOf[java.util.Map[String, String]]
      createdTime = String.valueOf(gen.get("createdTime"))
      for (i <- 0 until propsJava.size()) {
        val mapKey: Utf8 = new Utf8(String.valueOf(i))
        val attrEach = propsJava.get(mapKey).asInstanceOf[Utf8].toString
        attrs += attrEach + ","
      }
      attrs = attrs.substring(0, attrs.length - 1)
      attrs
    }


  }
}
