package com.free4lab.sparktest

import java.io.File
import java.sql.{Connection, PreparedStatement}

import com.free4lab.sparktest.constants.{CommonMethods, Con}
import com.free4lab.sparktest.operation.{MysqlOperation, WriteHDFS}
import kafka.AvroProducer2
import org.apache.spark.api.java.JavaRDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.IntegerType
import org.deeplearning4j.nn.api.OptimizationAlgorithm
import org.deeplearning4j.nn.conf.NeuralNetConfiguration
import org.deeplearning4j.nn.conf.layers.{LSTM, RnnOutputLayer}
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.nn.weights.WeightInit
import org.deeplearning4j.optimize.listeners.ScoreIterationListener
import org.deeplearning4j.spark.impl.multilayer.SparkDl4jMultiLayer
import org.deeplearning4j.spark.impl.paramavg.ParameterAveragingTrainingMaster
import org.nd4j.linalg.activations.Activation
import org.nd4j.linalg.dataset.DataSet
import org.nd4j.linalg.factory.Nd4j
import org.nd4j.linalg.learning.config.Adam
import org.nd4j.linalg.lossfunctions.LossFunctions

/**
 * @Author: fuhua
 * @Date: 2019-06-12 16:17
 *        处理完后发数据
 */
object SinKDDLSTMBatching {
  def main(args: Array[String]): Unit = {
    /*
     * 以下是批处理
     */
    val fileName = "from0test.csv"
//            val fileName = "kddres2.csv"

    val header = ",4,5,35,36,41\n"
    val attrLength = header.split(",").length - 2
        val spark = SparkSession.builder().master("spark://ambari-namenode.com:7077").appName("sinBatching").getOrCreate()
//    val spark = SparkSession.builder().master("local[2]").appName("test").getOrCreate()

    val rownum = MysqlOperation.readValue("rownumber", "rownumber", "id=1")
    val unit = MysqlOperation.readValue("rownumber", "rownumber", "id=2")

    import spark.implicits._
    val datasetReadCsv = spark.read.format("csv").option("header", "true").load(Con.FILE_PATH + fileName)

    val datasetRead = datasetReadCsv.orderBy($"_c0".cast(IntegerType).desc).limit((unit + 1).toInt).orderBy($"_c0".asc).map(_.toSeq.foldLeft("")(_ + "," + _).substring(1))

    import org.apache.spark.sql.functions._
    val datasetOr = datasetRead.withColumn("id", monotonically_increasing_id())

    val datasetOr2 = datasetOr
    datasetOr.createOrReplaceTempView("dataset")
    datasetOr2.createOrReplaceTempView("dataset2")
    val datasetResult = spark.sql("select dataset.id,dataset.value as features,dataset2.value as labels from dataset inner join dataset2 on dataset.id = dataset2.id-1")
    println(datasetResult.count())

    if (datasetRead.count() < unit) {
      send(rownum.toInt)
      return
    }

    val rddResult: JavaRDD[DataSet] = datasetResult.rdd.map(row => {
      val featuresOr = row(1).asInstanceOf[String].split(",")
      val labelsOr = row(2).asInstanceOf[String].split(",")
      val features = Nd4j.create(1, featuresOr.length - 2, 1)
      val labels = Nd4j.create(1, labelsOr.length - 2, 1)
      for (i <- 1 until featuresOr.length - 1) {
        features.putScalar(Array(1, i - 1, 1), featuresOr(i).toDouble)
        labels.putScalar(Array(1, i - 1, 1), labelsOr(i).toDouble)
      }
      new DataSet(features, labels)
    })

    val tbpttLength = 50 //Length for truncated backpropagation through time. i.e., do parameter updates ever 50 characters
    val averagingFrequency = 3
    val examplesPerDataSetObject = 1
    val batchSizePerWorker = 50
    val numEpochs = 1

    val lstmLayer1Size = 20
    val lstmLayer2Size = 40
    val conf = new NeuralNetConfiguration.Builder()
      .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
      .seed(123)
      .l2(0.001)
      .weightInit(WeightInit.XAVIER)
      .updater(new Adam(0.002))
      .list()
      .layer(0, new LSTM.Builder().nIn(attrLength).nOut(lstmLayer1Size)
        .activation(Activation.IDENTITY).build())
      .layer(1, new LSTM.Builder().nIn(lstmLayer1Size).nOut(lstmLayer2Size)
        .activation(Activation.IDENTITY).build())
      .layer(2, new RnnOutputLayer.Builder(LossFunctions.LossFunction.MSE).activation(Activation.IDENTITY)
        .nIn(lstmLayer2Size).nOut(attrLength).build())
      .build()


    val tm = new ParameterAveragingTrainingMaster.Builder(examplesPerDataSetObject)
      .workerPrefetchNumBatches(2) //Asynchronously prefetch up to 2 batches
      .averagingFrequency(averagingFrequency)
      .batchSizePerWorker(batchSizePerWorker)
      .build()

    val keepTraining = false //TODO:为true则会找之前模型继续训练，rownumber数量将是每次取的数量
    var historyNet: MultiLayerNetwork = null
    if (keepTraining) {
      historyNet = CommonMethods.loadLSTMModel(Con.LSTM_FILE_NAME)
    }
    var sparkNetwork: SparkDl4jMultiLayer = null
    if (historyNet != null) {
      println("将使用之前模型继续训练！")
      sparkNetwork = new SparkDl4jMultiLayer(spark.sparkContext, historyNet, tm)
    } else {
      println("开始新的训练！")
      sparkNetwork = new SparkDl4jMultiLayer(spark.sparkContext, conf, tm)
    }

    sparkNetwork.setListeners(new ScoreIterationListener(1))


    //    val Array(trainingData,testingData) = rddResult.randomSplit(Array(0.8,0.2))
    for (j <- 0 until numEpochs) {

      //      val net:MultiLayerNetwork = sparkNetwork.fit(trainingData)
      println("开始训练！！！！")
      val net: MultiLayerNetwork = sparkNetwork.fit(rddResult)
      println("训练结束！！！！")

      val fileCrc: File = new File(Con.TEMP_PATH + "." + Con.LSTM_FILE_NAME + ".crc")
      if (fileCrc.exists()) {
        System.out.println("删除crc文件！")
        fileCrc.delete()
      }
      val file: File = new File(Con.TEMP_PATH + Con.LSTM_FILE_NAME)
      net.save(file, true)
      WriteHDFS.saveFile(Con.TEMP_PATH + Con.LSTM_FILE_NAME, Con.LSTM_FILE_NAME)
      //            val trainEvaluation:Evaluation = sparkNetwork.evaluate(trainingData)
      //      val trainAccuracy = trainEvaluation.accuracy()
      //      val testEvaluation:Evaluation = sparkNetwork.evaluate(testingData)
      //      val testAccuracy = testEvaluation.accuracy()
      //      println(trainAccuracy+" "+testAccuracy)
      println("train finish")

      //
//                  val predictValue2 = Nd4j.zeros(1, attrLength, 1)
//                  val temp2 = "10510,53.02576209825161,423.46655953536066,336.1201155609625,126.31198050254885,normal."
//                  val temp2s = temp2.split(",")
//                  for (i <- 1 until temp2s.length-1) {
//                    predictValue2.putScalar(Array(1, i - 1, 1), temp2s(i).toDouble)
//                  }
//                  println()
//
//
//                  val output2 = net.output(predictValue2)
//                  for (i <- 1 until temp2s.length-1) {
//                    print(output2.getFloat(Array(1, i - 1, 1)) + " ")
//                  }


      send(rownum.toInt)
    }
  }

  def send(rownum: Int): Unit = {
    println("开始发送：" + rownum)
    AvroProducer2.syncSend(rownum)
    var conn: Connection = null
    var ps: PreparedStatement = null
    try {
      conn = MysqlOperation.getConnect
    }
    try {
      ps = conn.prepareStatement("update rownumber set rownumber=? where id=1")
      ps.setDouble(1, rownum + 1441)
      ps.executeUpdate()
      println("发送完成：" + rownum)
    }
    MysqlOperation.closeConnect(conn, ps, null)
  }
}
