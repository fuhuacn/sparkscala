package com.free4lab.sparktest

import org.apache.spark.sql.SparkSession

/**
  * @Author: fuhua
  * @Date: 2019-06-13 14:01
  *       暂时不用
  */
object TestStreamingStruc {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder
      .appName("StructuredNetworkTest")
      .getOrCreate()
    import spark.implicits._

    // Create DataFrame representing the stream of input lines from connection to localhost:9999
    val lines = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "192.168.34.60:9092,192.168.34.62:9092,192.168.34.57:9092,192.168.34.79:9092,192.168.34.80:9092")
      .option("subscribe", "my-topic2")
      .load()

    lines.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
      .as[(String, String)]

    // Split the lines into words
    val words = lines.as[String].flatMap(_.split(" "))

    // Generate running word count
    val wordCounts = words.groupBy("value").count()
  }
}
