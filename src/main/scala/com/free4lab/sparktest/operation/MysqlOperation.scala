package com.free4lab.sparktest.operation

import java.sql.{Connection, DriverManager, ResultSet, Statement}

import com.free4lab.sparktest.constants.Con
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

/**
 * @Author: fuhua
 * @Date: 2019/9/17 3:40 下午
 */
object MysqlOperation {
  def getConnect: Connection = {
    var conn: Connection = null
    conn = DriverManager.getConnection("jdbc:mysql://daas.free4inno.com:6033/sparkml", "root", "MYSQL@free4inno")
    conn
  }

  def closeConnect(conn: Connection, stmt: Statement, rs: ResultSet): Unit = {
    if (rs != null) {
      rs.close()
    }
    if (stmt != null) {
      stmt.close()
    }
    if (conn != null) {
      conn.close()
    }
  }

  def readValue(table: String, attrName: String, query: String): Double = {
    var wssd: Double = 0.0

    var conn: Connection = null
    var ps: Statement = null
    var rs: ResultSet = null
    try {
      conn = getConnect
      ps = conn.createStatement()
      rs = ps.executeQuery(s"select `$attrName` from `$table` where $query")
      rs.next()
      wssd = rs.getDouble(1)
      wssd
    } catch {
      case e: Exception => e.printStackTrace()
        0.0
    } finally {
      closeConnect(conn,ps,rs)
    }
  }

  def writeDF(df: DataFrame, table: String, mode: SaveMode): Unit = {
    df.write.format("jdbc").mode(mode).option("url", Con.MYSQL).option("dbtable", table).option("user", Con.MYSQL_USER).option("password", Con.MYSQL_PASS).option("driver", "com.mysql.jdbc.Driver").save()
  }

  def readDF(spark: SparkSession, table: String): DataFrame = {
    spark.read.format("jdbc").option("url", Con.MYSQL).option("dbtable", table).option("user", Con.MYSQL_USER).option("password", Con.MYSQL_PASS).option("driver", "com.mysql.jdbc.Driver").load()
  }

}
