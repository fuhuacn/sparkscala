package com.free4lab.sparktest.operation;

import com.free4lab.sparktest.constants.Con;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * @Author: fuhua
 * @Date: 2019-06-27 23:09
 */

public class WriteHDFS {
    private final static Logger logger = LoggerFactory.getLogger(WriteHDFS.class);

    final static String hdfs = Con.HDFS_PATH();

    public static void createFile(String fileName, String header) {
        FileSystem fileSystem;
        try {
            Configuration conf = config();
            fileSystem = FileSystem.get(conf);
            Path path = new Path(hdfs + fileName);
            if (fileSystem.exists(path)) {
                logger.info("文件存在，可以进行以下操作。");
            } else {
                logger.warn("文件不存在，创建文件后进行操作");
                fileSystem.create(path);
                fileSystem.close();
                fileSystem = FileSystem.get(conf);
                OutputStream out = fileSystem.append(path);
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
                bw.write(header);
                bw.flush();
                bw.close();
                out.flush();
                out.close();
                fileSystem.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveFile(String location, String fileName) {
        FileSystem fileSystem=null;
        try {
            Configuration conf = config();
            fileSystem = FileSystem.get(conf);
            Path path = new Path(hdfs + fileName);
            Path srcPath = new Path(location);
            fileSystem.copyFromLocalFile(true, srcPath, path);
            System.out.println("拷贝到hdfs完成");
            fileSystem.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Boolean readFile(String locationFullName, String fileNameInHdfs) {
        System.out.println("read file in");
        FileSystem fileSystem;
        try {
            Configuration conf = config();
            fileSystem = FileSystem.get(conf);
            Path hdfsPath = new Path(hdfs + fileNameInHdfs);
            Path localPath = new Path(locationFullName);
            fileSystem.copyToLocalFile(hdfsPath, localPath);
            System.out.println("拷贝到本地完成"+locationFullName);
            fileSystem.close();
        } catch (FileNotFoundException e) {
            System.out.println("No train model now");
            return false;
        } catch (IOException e) {
            System.out.println("在从hdfs拷贝中出现了问题");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static String readLineHdfs(String fileName, int lineNumber) {
        FileSystem fileSystem;
        try {
            Configuration conf = config();
            fileSystem = FileSystem.get(conf);
            Path path = new Path(hdfs + fileName);
            if (!fileSystem.exists(path)) {
                logger.error("文件不存在，无法读取");
                return "";
            }
            FSDataInputStream input = fileSystem.open(path);
            InputStreamReader inr = new InputStreamReader(input);
            BufferedReader read = new BufferedReader(inr);
            int i = 1;
            String line;
            while ((line = read.readLine()) != null) {
                i++;
                if (lineNumber < i) {
                    fileSystem.close();
                    return line;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.error("why nothing!!!!");
        return "";
    }

    public static String readManyLineHdfs(String fileName, int startLine, int endLine) {
        FileSystem fileSystem;
        try {
            Configuration conf = config();
            fileSystem = FileSystem.get(conf);
            Path path = new Path(hdfs + fileName);
            if (!fileSystem.exists(path)) {
                logger.error("文件不存在，无法读取");
                return "";
            }
            FSDataInputStream input = fileSystem.open(path);
            InputStreamReader inr = new InputStreamReader(input);
            BufferedReader read = new BufferedReader(inr);
            int i = 1;
            String line;
            String res = "";
            while ((line = read.readLine()) != null) {
                i++;
                if (startLine < i) {
                    res += line + "\n";
                }
                if (i > endLine) {
                    fileSystem.close();
                    return res.substring(0, res.length() - 1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.error("why nothing!!!!");
        return "";
    }

    public static Configuration config() {
        Configuration conf = new Configuration();
        conf.set("fs.defaultFS", hdfs);
        conf.set("dfs.client.block.write.replace-datanode-on-failure.policy", "NEVER");
        conf.set("dfs.client.block.write.replace-datanode-on-failure.enable", "true");
        conf.setBoolean("fs.hdfs.impl.disable.cache", true);
        return conf;
    }

    public static void main(String[] args) {
        Long begin = System.currentTimeMillis();
        System.out.println(readManyLineHdfs("2.csv", 1, 2));
        System.out.println(System.currentTimeMillis() - begin);
    }
}
